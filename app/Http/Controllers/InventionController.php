<?php

namespace App\Http\Controllers;

use App\invention;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\newInvention;
use App\User;
use App\Note;
use JWTAuth;
use App\Jobs\sendNewInventionToInvestor;

class InventionController extends Controller
{
                    // when submit for review remember to validate cac_no (unique:cac_no) and business name,email 
    //userType=>0:inventor,1:investor
    //invention table <=> status:
        // 0->default(no invention submited yet),
        // 1->submited(invention is under review), 
        // 2-cancel, 3->accepted, 
        // -1->rejected, 
    
    //notes table <=> status:0->default, -1->rejected, 2->cancelled 3->accepted
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    public function getInvention(Request $req, $invention_id){
        $auth = Auth::user($req->query('token'));
        $invention = invention::find($invention_id);
        if(!$invention){
            return response(['status'=>false, 'msg'=>'No invention found']);
        }
        return response(['status'=>true, 'data'=>$invention]);
    }

    public function getUserInventions(Request $req){
        $auth=Auth::user($req->query('token'));
        $invent=invention::where('user_id', $auth->id)->get();
        if(count($invent) < 1){
            return response(['status'=>false,'msg'=>'No invention found']);
        }
        return response(['status'=>true,'data'=>$invent]);
    }

    public function getSavedData(Request $request){
        $data=invention::where('user_id',Auth::user($request->query('token'))->id)->latest()->first();
        return response(['data'=>$data]);
    }

    public function validateSubmit($request,$key){
        $this->validate($request, [
            $key=>'required|string'
        ]);
    }
   
    public function submitForReview(Request $request){

        $auth=Auth::user($request->query('token')); 
        $inputs=$request->input();
        if($inputs['id'] == 0){
            $invention = new invention();
        }else{
            $invention = invention::find($inputs['id']);
            if(!$invention){
                return response(['status'=>false, 'msg'=>'Invention not found']);
            }
        }
        $invention->user_id = $auth->id;

        if(!isset($inputs['email'])){
            return response(['status'=>false, 'msg'=>'Email field is required']);
        }else{
            $this->validate($request,[
                "email"=>'required|email'
            ]);
            $invention->email = $inputs['email']; 
        }
        if(!isset($inputs['business_name'])){
            return response(['status'=>false, 'msg'=>'Business name field is required']);
        }else{
            $this->validateSubmit($request,'business_name');
            $invention->business_name = $inputs['business_name']; 
        }
        if(!isset($inputs['cac_no'])){
            return response(['status'=>false, 'msg'=>'CAC number field is required']);
        }else{
            $this->validateSubmit($request,'cac_no');
            $invention->cac_no = $inputs['cac_no']; 
        }
        if(!isset($inputs['business_contact'])){
            return response(['status'=>false, 'msg'=>'Business contact field is required']);
        }else{
            $this->validateSubmit($request,'business_contact');
            $invention->business_contact = $inputs['business_contact']; 
        }
        if(!isset($inputs['year_reg'])){
            return response(['status'=>false, 'msg'=>'Year of registration field is required']);
        }else{
            $this->validateSubmit($request,'year_reg');
            $invention->year_reg = $inputs['year_reg']; 
        }
        if(!isset($inputs['industry'])){
            return response(['status'=>false, 'msg'=>'Industry field is required']);
        }else{
            $this->validateSubmit($request,'industry');
            $invention->industry = $inputs['industry']; 
        }
        if(!isset($inputs['business_address'])){
            return response(['status'=>false, 'msg'=>'Business Address field is required']);
        }else{
            $this->validateSubmit($request,'business_address');
            $invention->business_address = $inputs['business_address']; 
        }
        if(!isset($inputs['business_goal'])){
            return response(['status'=>false, 'msg'=>'Business goal field field is required']);
        }else{
            $this->validateSubmit($request,'business_goal');
            $invention->business_goal = $inputs['business_goal']; 
        }
        if(!isset($inputs['business_vision'])){
            return response(['status'=>false, 'msg'=>'Business vision field is required']);
        }else{
            $this->validateSubmit($request,'business_vision');
            $invention->business_vision = $inputs['business_vision']; 
        }
        if(!isset($inputs['business_mission'])){
            return response(['status'=>false, 'msg'=>'Business mission field is required']);
        }else{
            $this->validateSubmit($request,'business_mission');
            $invention->business_mission = $inputs['business_mission']; 
        }
        if(!isset($inputs['description'])){
            return response(['status'=>false, 'msg'=>'Description field is required']);
        }else{
            $this->validateSubmit($request,'description');
            $invention->description = $inputs['description']; 
        }
        if(!isset($inputs['target_market'])){
            return response(['status'=>false, 'msg'=>'Target market field is required']);
        }else{
            $this->validateSubmit($request,'target_market');
            $invention->target_market = $inputs['target_market']; 
        }
        if(!isset($inputs['business_stage'])){
            return response(['status'=>false, 'msg'=>'Business stage field is required']);
        }else{
            $this->validateSubmit($request,'business_stage');
            $invention->business_stage = $inputs['business_stage']; 
        }
        if(!isset($inputs['no_shareholder'])){
            return response(['status'=>false, 'msg'=>'Number of shareholder field is required']);
        }else{
            $this->validate($request,[
                "no_shareholder"=>'required|numeric'
            ]);
            $invention->no_shareholder = $inputs['no_shareholder']; 
        }
        if(!isset($inputs['no_employee'])){
            return response(['status'=>false, 'msg'=>'Number of employee field is required']);
        }else{
            $this->validate($request,[
                "no_employee"=>'required|numeric'
            ]);
            $invention->no_employee = $inputs['no_employee']; 
        }
        if(!isset($inputs['value_of_business'])){
            return response(['status'=>false, 'msg'=>'Value of business field is required']);
        }else{
            $this->validate($request,[
                "value_of_business"=>'required|numeric'
            ]);
            $invention->value_of_business = $inputs['value_of_business']; 
        }
        if(!isset($inputs['invest_required'])){
            return response(['status'=>false, 'msg'=>'Investment field is required']);
        }else{
            $this->validate($request,[
                "invest_required"=>'required|numeric'
            ]);
            $invention->invest_required = $inputs['invest_required']; 
        }
        if(!isset($inputs['equity'])){
            return response(['status'=>false, 'msg'=>'Equity field is required']);
        }else{
            $this->validate($request,[
                "equity"=>'required|numeric|min:0|max:100'
            ]);
            $invention->equity = $inputs['equity']; 
        }
        if(!isset($inputs['business_pitch'])){
            return response(['status'=>false, 'msg'=>'Pitch url field is required']);
        }else{
            $this->validate($request,[
                "business_pitch"=>'url'
            ]);
            $invention->business_pitch = $inputs['business_pitch']; 
        }


        if($request->hasFile('cac_certificate')){
            $invention->addMedia($request->cac_certificate)->toMediaCollection('cac_certificate');
        }else{
            if(!$invention->getMedia('cac_certificate')->last()){
                return response(['status'=>false,'msg'=>'Upload CAC certificate']);
            }
        }


        if($request->hasFile('director_id')){
            $invention->addMedia($request->director_id)->toMediaCollection('director_id');
        }else{
            if(!$invention->getMedia('director_id')->last()){
                return response(['status'=>false,'msg'=>'Upload Director id']);
            }
        }


        if($request->hasFile('business_plan')){
            $invention->addMedia($request->business_plan)->toMediaCollection('business_plan');
        }
        if($request->hasFile('other_document')){
            $invention->addMedia($request->other_document)->toMediaCollection('other_document');
        }

        $payload=[
            'email'=>$inputs['email'],
            'business_name'=>$inputs['business_name'],
        ];

        Mail::to(env('MAIL_USERNAME'))->sendNow(new newInvention($payload));

        $invention->status = 1;
        //if invention is being resubmited for review (due to maybe it was at first rejected) change the note status to 0
        $note = Note::where('invention_id',$invention->id)
                    ->where('status',2)
                    ->first();
        if($note){
            $note->status = 0;
            $note->save();
        }
        if(!$invention->save()){
            return response(['status'=>false, 'msg'=>'Error submitting for review, pls try again']);
        }
        return response(['status'=>true, 'msg'=>'Invention Submitted successfully'],200);
    }

    public function saveProgress(Request $request){
        $auth=Auth::user($request->query('token')); //this generate error by itself if token is not correct
        $inputs=$request->input();

        if($inputs['id'] == 0){
            $invention = new invention();
        }else{
            $invention = invention::find($inputs['id']);
            if(!$invention){
                return response(['status'=>false, 'msg'=>'Invention not found']);
            }
        }
        $invention->user_id = $auth->id;
        if(isset($inputs['email'])){
            $this->validate($request,[
                "email"=>'email'
            ]);
            $invention->email = $inputs['email']; 
        }
        if(isset($inputs['business_name'])){
            $this->validate($request,[
                "business_name"=>'string'
            ]);
            $invention->business_name = $inputs['business_name']; 
        }
        if(isset($inputs['cac_no'])){
            $this->validate($request,[
                "cac_no"=>'string'
            ]);
            $invention->cac_no = $inputs['cac_no']; 
        }
        if(isset($inputs['business_contact'])){
            $this->validate($request,[
                "business_contact"=>'string'
            ]);
            $invention->business_contact = $inputs['business_contact']; 
        }
        if(isset($inputs['year_reg'])){
            $this->validate($request,[
                "year_reg"=>'string'
            ]);
            $invention->year_reg = $inputs['year_reg']; 
        }
        if(isset($inputs['industry'])){
            $this->validate($request,[
                "industry"=>'string'
            ]);
            $invention->industry = $inputs['industry']; 
        }
        if(isset($inputs['business_address'])){
            $this->validate($request,[
                "business_address"=>'string'
            ]);
            $invention->business_address = $inputs['business_address']; 
        }
        if(isset($inputs['business_goal'])){
            $this->validate($request,[
                "business_goal"=>'string'
            ]);
            $invention->business_goal = $inputs['business_goal']; 
        }
        if(isset($inputs['business_vision'])){
            $this->validate($request,[
                "business_vision"=>'string'
            ]);
            $invention->business_vision = $inputs['business_vision']; 
        }
        if(isset($inputs['business_mission'])){
            $this->validate($request,[
                "business_mission"=>'string'
            ]);
            $invention->business_mission = $inputs['business_mission']; 
        }
        if(isset($inputs['description'])){
            $this->validate($request,[
                "description"=>'string'
            ]);
            $invention->description = $inputs['description']; 
        }
        if(isset($inputs['target_market'])){
            $this->validate($request,[
                "target_market"=>'string'
            ]);
            $invention->target_market = $inputs['target_market']; 
        }
        if(isset($inputs['business_stage'])){
            $this->validate($request,[
                "business_stage"=>'string'
            ]);
            $invention->business_stage = $inputs['business_stage']; 
        }
        if(isset($inputs['no_shareholder'])){
            $this->validate($request,[
                "no_shareholder"=>'required|numeric'
            ]);
            $invention->no_shareholder = $inputs['no_shareholder']; 
        }
        if(isset($inputs['no_employee'])){
            $this->validate($request,[
                "no_employee"=>'required|numeric'
            ]);
            $invention->no_employee = $inputs['no_employee']; 
        }
        if(isset($inputs['value_of_business'])){
            $this->validate($request,[
                "value_of_business"=>'required|numeric'
            ]);
            $invention->value_of_business = $inputs['value_of_business']; 
        }
        if(isset($inputs['invest_required'])){
            $this->validate($request,[
                "invest_required"=>'required|numeric'
            ]);
            $invention->invest_required = $inputs['invest_required']; 
        }
        if(isset($inputs['equity'])){
            $this->validate($request,[
                "equity"=>'required|numeric|min:0|max:100'
            ]);
            $invention->equity = $inputs['equity']; 
        }
        if(isset($inputs['business_pitch'])){
            $this->validate($request,[
                "business_pitch"=>'url'
            ]);
            $invention->business_pitch = $inputs['business_pitch']; 
        }
        if($request->hasFile('cac_certificate')){
            $invention->addMedia($request->cac_certificate)->toMediaCollection('cac_certificate');
        }
        if($request->hasFile('director_id')){
            $invention->addMedia($request->director_id)->toMediaCollection('director_id');
        }
        if($request->hasFile('business_plan')){
            $invention->addMedia($request->business_plan)->toMediaCollection('business_plan');
        }
        if($request->hasFile('other_document')){
            $invention->addMedia($request->other_document)->toMediaCollection('other_document');
        }



        if(!$invention->save()){

            //Attaching categories to invention
            if(isset($inputs["categories"])){
                //Detach already added categories
                foreach ($invention->category()->get() as $cat){
                    $invention->category()->detach($cat->id);
                }

                //Attach new ones
                foreach (explode(',',$inputs['categories']) as $cat){
                    $invention->category()->attach($cat);
                }
            }
            //Attaching categories to invention ends here

            return response(['status'=>false, 'msg'=>'Error saving progress, pls try again']);
        }



        return response(['status'=>true, 'msg'=>'Progress saved successfully'],200);
       
    }


    
}
