<?php

namespace App\Http\Controllers;

use App\invention;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
// use App\inventor;
use App\User;
use JWTAuth;

class InventorController extends Controller
{
                            // when submit for review remember to validate cac_no (unique:cac_no) and business name,email 
    //userType=>0:inventor,1:investor

    public function __construct()
    {
        $this->middleware('jwt.auth');
    }
    public function getSavedData(Request $request){
        $data=inventor::where('user_id',Auth::user($request->query('token'))->id)->first();
        return response(['data'=>$data]);
    }
    public function saveProgress(Request $request){
        $auth=Auth::user($request->query('token'));

        $a=$request->only('business_name','cac_no','email','business_contact','year_reg','industry','business_address','business_goal',
            'business_vision','business_mission','description','target_market','business_stage','no_shareholder','no_employee',
            'value_of_business','invest_required','equity','cac_certificate','director_id','business_plan','business_pitch','other_document'
        );
        // if(!isset($a['cac_certificate'])){
        //     return 'cac not';
        // }
        if(isset($a['email'])){
            $this->validate($request,[
                "email"=>'email'
            ]);
        }
        if(isset($a['cac_certificate'])){
            $auth->addMedia($a['cac_certificate'])->toMediaCollection();
        }
        if(isset($a['director_id'])){
            
        }
        if(isset($a['business_plan'])){
            
        }
        if(isset($a['other_document'])){
            
        }
        $data = inventor::updateOrCreate(
            ['user_id' => $auth->id],
            $a
        );
        return response(['data'=>$data,'status'=>true, 'msg'=>'Progress saved successfully'],200);
    }


}
