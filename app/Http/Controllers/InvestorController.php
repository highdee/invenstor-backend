<?php

namespace App\Http\Controllers;

use App\category;
use App\invention;
use App\review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

class InvestorController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('jwt.auth');
        $user=JWTAuth::parseToken()->toUser();
//        if($user->userType != 1){
//            return response([],404);
//            exit(0);
//        }
    }

    public function fetchInventions(Request $request){
        $inventions=invention::where('status',3);

        if($request->has('category')) {
            $category = category::where('id',$request->input('category'))->first();
            $category_inventions=$category->getInventionIds();
            $inventions = $inventions->whereIn('id',$category_inventions);
        }

        if($request->has('rating')) {
            $ids = review::where('rating',$request->input('rating'))->pluck('invention_id')->toArray();
            $inventions = $inventions->whereIn('id',$ids);
        }

        if($request->has('text')){
            $inventions = $inventions->where('business_name','like','%'.$request->input('text').'%');
        }

        if($request->has('min')){
            $inventions = $inventions->where('invest_required','>=',$request->input('min'));
        }

        if($request->has('max')){
            $inventions = $inventions->where('invest_required','<=',$request->input('max'));
        }

        $inventions=$inventions->paginate();
        return response($inventions,200);
    }

    public function attachCategory($id){
        $user=JWTAuth::parseToken()->toUser();

        $user->categories()->detach($id);
        $user->categories()->attach($id);

        return response([
            'status'=>true,
            'message'=>'Category was added successfully'
        ],200);
    }
    public function detachCategory($id){
        $user=JWTAuth::parseToken()->toUser();

        $user->categories()->detach($id);

        return response([
            'status'=>true,
            'message'=>'Category was removed successfully'
        ],200);
    }
}
