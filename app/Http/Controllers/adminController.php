<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Jobs\sendNewInventionToInvestor;
use App\Mail\rejectInvention;
use App\Mail\cancelInvention;
use App\Mail\acceptInvention;
use App\User;
use App\invention;
use Auth;
use App\Note;
use App\review;
use Illuminate\Support\Facades\DB;

class adminController extends Controller
{
             // when submit for review remember to validate cac_no (unique:cac_no) and business name,email 
    //userType=>0:inventor,1:investor
    //invention table <=> status:0->default(no invention submited yet),1->submited(invention is under review), 2-cancel, 3->accepted, -1->rejected, 
    //notes table <=> status:0->default, -1->rejected, 2->cancelled, 3-accepted
    public function __construct() {
        $this->middleware('auth:admin');
    } 

    

    public function download($type,invention $id){
        return response()->download($id->getMedia($type)->last()->getPath());
    }

    public function logout() {
        Auth::logout();
        return redirect('/adminLogin');
    }
    public function inventor_show($user_id){
        $data = User::where('id',$user_id)->first();
        if(!$data){
            session()->flash('message', 'Inventor not found');
            session()->flash('type', 'error');
            return redirect()->back(); 
        }
        // dd($data->inventions);
        return view('admin.inventor_show')->with('invent',$data);
    }

    public function rejectInvention(Request $request){
        $invention=invention::find($request->inventionId);
        if(!$invention){
            session()->flash('message', 'Invention not found');
            session()->flash('type', 'error');
            return redirect()->back(); 
        }
        $this->validate($request,[
            'note'=>'required|string'
        ]);
        $note = new Note();
        $note->invention_id = $request->inventionId;
        $note->note = $request->note;

        $note->status = -1;
        $invention->status = -1;

        $payload=[
            'note'=>$request['note']
        ];

        Mail::to($invention->email)->sendNow(new rejectInvention($payload));
        if(!$note->save() || !$invention->save()){
            session()->flash('message', 'Error rejecting invention, pls try again');
            session()->flash('type', 'error');
            return redirect()->back(); 
        }
        session()->flash('message', 'Invention rejected successful');
        session()->flash('type', 'success');
        return redirect()->back();
    }

    public function cancelInvention(Request $request){
        $invention=invention::find($request->inventionId);
        if(!$invention){
            session()->flash('message', 'Invention not found');
            session()->flash('type', 'error');
            return redirect()->back(); 
        }
        $this->validate($request,[
            'note'=>'required|string'
        ]);
        $note = new Note();
        $note->invention_id = $request->inventionId;
        $note->note = $request->note;

        $note->status = 2;
        $invention->status = 2;

        
        $payload=[
            'note'=>$request['note']
        ];

        Mail::to($invention->email)->sendNow(new cancelInvention($payload));

        if(!$note->save() || !$invention->save()){
            session()->flash('message', 'Error cancelling invention, pls try again');
            session()->flash('type', 'error');
            return redirect()->back(); 
        }
        session()->flash('message', 'Invention canceled successful');
        session()->flash('type', 'success');
        return redirect()->back();
    }

    public function acceptInvention(Request $request){
        $this->validate($request, [
            'inventionId'=>'required|integer',
            'review'=>'required|string',
            'rating'=>'required|integer',
        ]);
        $invention=invention::find($request->inventionId);
        if(!$invention){
            session()->flash('message', 'Invention not found');
            session()->flash('type', 'error');
            return redirect()->back(); 
        }
        $invention->status = 3;
        $review=new review;
        $review->parent_id=0;
        $review->invention_id=$request->inventionId;
        $review->user_id=$invention->user->id;
        $review->rating=$request->rating;
        $review->content=$request->review;
        $review->status=3;
        //send mail to inventor that invention has been added
        Mail::to($invention->email)->sendNow(new acceptInvention());
        //send mail to all investor in this category that a new invention has been added
        sendNewInventionToInvestor::dispatch($invention->categories);

        if(!$invention->save() || !$review->save()){
            session()->flash('message', 'Error accepting invention, pls try again');
            session()->flash('type', 'error');
            return redirect()->back(); 
        }
        session()->flash('message', 'Invention accepted successful');
        session()->flash('type', 'success');
        return redirect()->back();
    }

    public function inventor(){
        $data = User::where('userType',0)->get();
        return view('admin.inventor')->with('inventors', $data);
    }
    public function inventions(){
        $data = invention::where('status','!=',0)->get();
        return view('admin.inventions')->with('inventions', $data);
    }

    public function invention($id){
        $data = invention::where('id',$id)
                        ->where('status','!=',0)
                        ->first();
        if(!$data){
            session()->flash('type','error');
            session()->flash('message','Invention not found');
            return redirect()->back();
        }
        return view('admin.invention')->with('invention',$data);
    }

    public function investors(){
        $data = User::where('userType',1)->get();
        return view('admin.investors')->with('investors', $data);
    }

    public function investor($user_id){
        $data = User::find($user_id);
        if (!$data){
            session()->flash('type','error');
            session()->flash('message','Investor not found');
            return redirect()->back();
        }
        return view('admin.investor')->with('investor', $data);
    }

    public function index(){
        $invention = count(invention::all());
        $inventor = count(User::where('userType',0)->get());
        $investor = count(User::where('userType',1)->get());
        return view('admin.index')->with(['invention'=>$invention, 'inventor'=>$inventor,'investor'=>$investor]);
    }
    public function userList(){
        $data = User::all();
        return view('admin.user')->with('users',$data);
    }
    public function user_show($id){
        $data = User::find($id);
        return view('admin.user_show')->with('user', $data);
    }
    public function deleteUser(Request $req){
        $this->validate($req,[
            'user_id'=>'required'
        ]);
        $data = User::find($req->user_id);
        if(!$data->delete()){
            session()->flash('message', 'User Delete Unsuccessful');
            session()->flash('type', 'error');
            return redirect()->back();  
        }
        session()->flash('message', 'User Delete Successful');
        session()->flash('type', 'success');
        return redirect()->route('admin.user'); 
    }

    public function banUser(Request $request){
        $data = User::find($request['user_id']);
        $data->status = 1 - $data->status;
        if(!$data->save()){
            session()->flash('message', 'User Ban Unsuccessfull');
            session()->flash('type', 'error');
            return redirect()->back(); 
        }
        session()->flash('message', 'Successful');
        session()->flash('type', 'success');
        return redirect()->back(); 
    }
}
