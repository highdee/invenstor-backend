<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\rejectInvention;
use App\Mail\cancelInvention;
use App\Mail\acceptInvention;
use App\User;
use App\invention;
use Auth;
use App\Note;

class adminController extends Controller
{
             // when submit for review remember to validate cac_no (unique:cac_no) and business name,email 
    //userType=>0:inventor,1:investor
    //invention table <=> status:0->default(no invention submited yet),1->submited(invention is under review), 2-cancel, 3->accepted, -1->rejected, 
    //notes table <=> status:0->default, -1->rejected, 2->cancelled, 3-accepted
    public function __construct() {
        $this->middleware('auth:admin');
    } 

    public function download($type,invention $id){
        return response()->download($id->getMedia($type)->last()->getPath());
    }

    public function logout() {
        Auth::logout();
        return redirect('/adminLogin');
    }
    public function inventor_show($user_id){
        $data = User::where('id',$user_id)->first();
        if(!$data){
            session()->flash('message', 'Inventor not found');
            session()->flash('type', 'error');
            return redirect()->back(); 
        }
        return view('admin.inventor_show')->with('invent',$data);
    }

    public function rejectInvention(Request $request){
        $invention=invention::find($request->inventionId);
        if(!$invention){
            session()->flash('message', 'Invention not found');
            session()->flash('type', 'error');
            return redirect()->back(); 
        }
        $this->validate($request,[
            'note'=>'required|string'
        ]);
        $note = new Note();
        $note->invention_id = $request->inventionId;
        $note->note = $request->note;

        $note->status = -1;
        $invention->status = -1;

        
        $payload=[
            'note'=>$request['note']
        ];

        Mail::to($invention->email)->sendNow(new rejectInvention($payload));
        if(!$note->save() || !$invention->save()){
            session()->flash('message', 'Error rejecting invention, pls try again');
            session()->flash('type', 'error');
            return redirect()->back(); 
        }
        session()->flash('message', 'Invention rejected successful');
        session()->flash('type', 'success');
        return redirect()->back();
    }

    public function cancelInvention(Request $request){
        $invention=invention::find($request->inventionId);
        if(!$invention){
            session()->flash('message', 'Invention not found');
            session()->flash('type', 'error');
            return redirect()->back(); 
        }
        $this->validate($request,[
            'note'=>'required|string'
        ]);
        $note = new Note();
        $note->invention_id = $request->inventionId;
        $note->note = $request->note;

        $note->status = 2;
        $invention->status = 2;

        
        $payload=[
            'note'=>$request['note']
        ];

        Mail::to($invention->email)->sendNow(new cancelInvention($payload));

        if(!$note->save() || !$invention->save()){
            session()->flash('message', 'Error cancelling invention, pls try again');
            session()->flash('type', 'error');
            return redirect()->back(); 
        }
        session()->flash('message', 'Invention canceled successful');
        session()->flash('type', 'success');
        return redirect()->back();
    }

    public function acceptInvention(Request $request){
        $invention=invention::find($request->inventionId);
        if(!$invention){
            session()->flash('message', 'Invention not found');
            session()->flash('type', 'error');
            return redirect()->back(); 
        }
        $invention->status = 3;
        Mail::to($invention->email)->sendNow(new acceptInvention());

        if(!$invention->save()){
            session()->flash('message', 'Error cancelling invention, pls try again');
            session()->flash('type', 'error');
            return redirect()->back(); 
        }
        session()->flash('message', 'Invention accepted successful');
        session()->flash('type', 'success');
        return redirect()->back();
    }

    public function inventor(){
        $data = User::where('userType',0)->get();
        return view('admin.inventor')->with('inventors', $data);
    }
    public function invention(){
        $data = invention::where('status','!=',0)->get();
        return view('admin.invention')->with('inventions', $data);
    }
    public function investor(){
        $data = User::where('userType',1)->get();
        return view('admin.investor')->with('investors', $data);
    }
    public function index(){
        $invention = count(invention::all());
        $inventor = count(User::where('userType',0)->get());
        $investor = count(User::where('userType',1)->get());
        return view('admin.index')->with(['invention'=>$invention, 'inventor'=>$inventor,'investor'=>$investor]);
    }
    public function userList(){
        $data = User::all();
        return view('admin.user')->with('users',$data);
    }
    public function user_show($id){
        $data = User::find($id);
        return view('admin.user_show')->with('user', $data);
    }
    public function deleteUser($id){
        $data = User::find($id);
        if(!$data->delete()){
            session()->flash('message', 'User Delete Unsuccessfull');
            session()->flash('type', 'error');
            return redirect()->back();  
        }
        session()->flash('message', 'User Delete Successfull');
        session()->flash('type', 'success');
        return redirect()->route('admin.user'); 
    }

    public function banUser(Request $request){
        $data = User::find($request['user_id']);
        $data->status = 1 - $data->status;
        if(!$data->save()){
            session()->flash('message', 'User Ban Unsuccessfull');
            session()->flash('type', 'error');
            return redirect()->back(); 
        }
        session()->flash('message', 'Successful');
        session()->flash('type', 'success');
        return redirect()->back(); 
    }
}
