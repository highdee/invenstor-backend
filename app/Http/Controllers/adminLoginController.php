<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class adminLoginController extends Controller
{
	public function __construct()
	{
		$this->middleware('guest:admin');
	}
    public function show(){
    	return view('admin.adminLogin');
    }
    
    public function login(Request $request){
    	$this->validate($request, [
    		'username'=>'required|string',
    		'password'=>'required'
    	]);
    	if(Auth::guard('admin')->attempt(['username'=>$request->username, 'password'=>$request->password])){
    		return redirect()->intended('/admin');
    	}else{
		
			session()->flash('message', 'Password and Email Incorrect');
        	session()->flash('type', 'error');
		}

    	return redirect()->back()->withInput($request->only('username'));
    }
}
