<?php

namespace App\Http\Controllers;

use App\Jobs\EmailJobs;
use Illuminate\Http\Request;
use App\Mail\verificationEmail;
use App\Mail\resetMail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use JWTAuth;
use App\User;

class loginController extends Controller{
    public function createAccount(Request $request){
        $this->validate($request,[
            'lastname'=>'required|string',
            'firstname'=>'required|string',
            'email'=>'required|email|unique:users',
            'password'=>'required',
            'userType'=>'required',
            'phone'=>'required'
        ]);
        
        $u = new User();
        $u->lastname = $request['lastname'];
        $u->firstname = $request['firstname'];
        $u->email = $request['email'];
        $u->password = bcrypt($request['password']);
        $u->phone=$request['phone'];
        $u->userType=$request['userType'];
        $u->remember_token=mt_rand(99999,99999999).str_random(12);

        $u->save();

        $link=env('APP_FRONTEND')."/verify-account?code=".$u->remember_token;

        EmailJobs::dispatch(['id'=>$u->id,'link'=>$link]);




        return response()->json([
            'status'=>true,
            'msg'=>'Registration Successful'
        ]);
    }

    public function verify_account(Request $request){
        $token=$request->query('code');

        if(!isset($token)){
            return response()->json([
                'status'=>false,
                'msg'=>"Token not found"
            ]);
        }


        $User=User::where('remember_token',$token)->first();

        if(!$User){
            return response()->json([
                'status'=>false,
                'msg'=>"Account not found"
            ]);
        }

        $User->email_verified_at=date('Y-m-d h:i:s');
        $User->remember_token=mt_rand(99999,99999999).str_random(12);
        $User->save();

        return response()->json([
            'status'=>true,
            'msg'=>"Account verified, pls login"
        ]);
    }

    public function login(Request $request){
        $this->validate($request,[
            'email'=>'required|email',
            'password'=>'required'
        ]);

        $credentials = $request->only('email', 'password');

        if($token=JWTAuth::attempt($credentials)){
            $user=User::where('id',auth::user($token)->id)->first();
            return response()->json([
                'status'=>true,
                'msg'=>"Login Successful",
                'token'=>$token,
                "user"=>$user
            ]);
        }else{
            return response()->json(
                [
                    "status"=>false,
                    "message"=>"Invalid login details"
                ]
            );
        }
    }

    public function resend_verification(Request $request,$id){
        $u = User::find($id);
        $u->remember_token=mt_rand(99999,99999999).str_random(12);
        $u->save();
        $link=env('APP_URL_FRONTEND')."/verify-account?code=".$u->remember_token;
        Mail::to($request['email'])->sendNow(new verificationEmail([
            'link'=>$link
        ]));
        return response()->json([
            'status'=>true,
            'msg'=>'Verification Link Sent Successful Successful'
        ]);
    }

    public function forget_password(Request $request){
        $this->validate($request,[
            'email'=>'required', 
        ]);

        $User=User::where('email',$request->input('email'))->first();

        if(!$User){
            return response()->json([
                'status'=>false,
                'msg'=>"Account not found"
            ]);
        }

        $User->remember_token=mt_rand(99999,99999999).str_random(12).mt_rand(99999,99999999).str_random(12).mt_rand(99999,99999999).str_random(12);
        $User->save();

        $link=env('APP_FRONTEND')."/reset-password/".$User->remember_token;
        Mail::to($User->email)->sendNow(new resetMail([
            'link'=> $link
        ]));

        return response()->json([
            'status'=>true,
            'msg'=>"Reset mail has been sent"
        ]);
    }

    public function verify_password(Request $request){
        $token=$request->query('code');

        if(!isset($token)){
            return response()->json(['status'=>false,'msg'=>"Token not found" ]);
        }

        $User=User::where('remember_token',$token)->first();

        if(!$User){
            return response()->json([
                'status'=>false,
                'msg'=>"Expired link"
            ]);
        }

        return response()->json(['status'=>true,'msg'=>"correct token" ]);
    }

    public function reset_password(Request $request){
        $this->validate($request,[
            'password'=>'required|confirmed',
            'token'=>'required'
        ]);

        $User=User::where('remember_token',$request->input('token'))->first();

        if(!$User){
            return response()->json([
                'status'=>false,
                'msg'=>"Account not found"
            ]);
        }

        $User->password=bcrypt($request->input('password'));
        $User->remember_token=mt_rand(99999,99999999).str_random(12);
        $User->save();

        return response()->json(['status'=>true,'msg'=>"Password reset was successful" ]);
    } 
}
