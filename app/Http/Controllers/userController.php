<?php

namespace App\Http\Controllers;

use App\invention;
use App\Jobs\sendReviewNotification;
use App\review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\verificationEmail;
use JWTAuth;
use App\User;

class userController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }


    public function get_dashboard(Request $request){
        $user=User::where('id',Auth::user($request->query('token'))->id)->first();
        return response([
            'user'=>$user,
        ],200);
    }

    public function refresh(){
        return $this->respondWithToken(auth()->refresh());
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

    public function resend_verification(Request $request){
        $user = Auth::user($request->query('token'));
        $user->remember_token=mt_rand(99999,99999999).str_random(12);
        $user->save();
        $link=env('APP_FRONTEND')."/verify-account?code=".$user->remember_token;
        Mail::to($user->email)->sendNow(new verificationEmail([
            'link'=>$link
        ]));
        return response()->json([
            'status'=>true,
            'msg'=>'Verification Link Sent Successful Successful'
        ]);
    }

    public function getInvention($reference){
        $invention=invention::where(['status'=>3,'reference_id'=>$reference])->first();
        if(!$invention){
            return response([
                'status'=>false,
                'message'=>''
            ],404);
        }
        return response([
            'status'=>true,
            'data'=>$invention
        ],200);

    }

    public function sendReview(Request $request,$reference){
        $user = Auth::user($request->query('token'));
        $invention=invention::where(['status'=>3,'reference_id'=>$reference])->first();
        if(!$invention){
            return response([
                'status'=>false,
                'message'=>''
            ],404);
        }

        $details=$request->input();

        $review=new review();
        $review->user_id=$user->id;
        $review->invention_id=$invention->id;
        $review->content=$details['content'];
        $review->rating=$details['rating'];
        $review->parent_id=0;
        $review->status=1;
        $review->save();

        $data=[
            'user'=>$invention->user()->first()->id,
            'invention'=>$invention->id,
            'review'=>$review->id,
            'reviewer'=>$review->user_id
        ];

        sendReviewNotification::dispatch($data);

        return response([
            'status'=>true,
            'data'=>$review
        ],200);
    }

    public function getReviews($reference){
        $invention=invention::where(['status'=>3,'reference_id'=>$reference])->first();
        if(!$invention){
            return response([
                'status'=>false,
                'message'=>''
            ],404);
        }


        $reviews=review::where(['status'=>1,'invention_id'=>$invention->id])->orderBy('id','desc')->paginate(10);

        return response($reviews,200);
    }
}
