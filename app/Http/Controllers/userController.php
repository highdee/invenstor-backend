<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\verificationEmail;
use JWTAuth;
use App\User;

class userController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    public function get_dashboard(Request $request){
        $user=User::where('id',Auth::user($request->query('token'))->id)->first();
        return response([
            'user'=>$user,
        ],200);
    }

    public function refresh(){
        return $this->respondWithToken(auth()->refresh());
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

    public function resend_verification(Request $request){
        $user = Auth::user($request->query('token'));
        $user->remember_token=mt_rand(99999,99999999).str_random(12);
        $user->save();
        $link=env('APP_FRONTEND')."/verify-account?code=".$user->remember_token;
        Mail::to($user->email)->sendNow(new verificationEmail([
            'link'=>$link
        ]));
        return response()->json([
            'status'=>true,
            'msg'=>'Verification Link Sent Successful Successful'
        ]);
    }
    
}
