<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Mail\sendMailToInvestor;
use Illuminate\Support\Facades\DB;

class sendNewInventionToInvestor implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $data=[];
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->data as $key => $value) {
            $investors=DB::table('category_user')->where(['category_id'=>$value['id']])->get();
            foreach ($investors as $k => $investor) {
                $user=User::find($investor->user_id);
                Mail::to($user->email)->sendNow(new sendMailToInvestor());
            }
        }
    }
}
