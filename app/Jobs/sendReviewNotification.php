<?php

namespace App\Jobs;

use App\invention;
use App\Mail\sendNotificationMail;
use App\review;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class sendReviewNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $data=[];
    public function __construct($data)
    {
        //
        $this->data=$data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user=User::find($this->data['user']);
        $reviewer=User::find($this->data['reviewer']);
        $invention=invention::find($this->data['invention']);
        $review=review::find($this->data['review']);
//
        $text="<div> <b>You have received a new review and rating on your invention from ".$reviewer->firstname."</b> </div>".
            "<p class='content'>".substr($review->content,0,100)."...</p>";
//
        $link=env('APP_FRONTEND').'/invention/'.$invention->reference_id.'/'.$invention->business_name;
        Mail::to($user->email)->sendNow(new sendNotificationMail(['name'=>$user->firstname,'content'=>$text,'link'=>$link,'link_text'=>'View review']));
    }
}
