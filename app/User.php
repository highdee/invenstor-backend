<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $appends=['inventions','categories'];

    public function getCategoriesAttribute(){
        return $this->categories()->get();
    }

    public function getInventionsAttribute(){
        return $this->inventions()->get();
    }

    public function inventions(){
        return $this->hasMany('App\invention','user_id','id');
    }

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token','created_at','updated_at'
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function categories(){
        return $this->belongsToMany('App\category','category_user');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

}
