<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    //



    public function getInventionIds(){
        return $this->inventions()->pluck('invention_id')->toArray();
    }
    public function inventions(){
        return $this->belongsToMany('App\invention','category_invention');
    }
}
