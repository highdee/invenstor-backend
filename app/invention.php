<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use PHPUnit\Framework\Constraint\Count;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\File;
use Spatie\MediaLibrary\Models\Media;

class invention extends Model implements HasMedia
{
//    STATUS 0 = NOT SUBMITTED YET
//    STATUS 1 = UNDER REVIEW
//    STATUS 2 = CANCELED
//    STATUS 3 = ACCEPTED


    use HasMediaTrait;
    protected $guarded=[];

    protected $appends=['reg_year','investment_raised','rating','categories','cac_certificate','director_id','business_plan','other_document','notes'];

    public function getRegYearAttribute(){
        return date('Y',strtotime($this->year_reg));
    }
    public function getInvestmentRaisedAttribute(){
        return 0;
    }

    public function getRatingAttribute(){
        $ratings=$this->reviews()->pluck('rating')->toArray();
        if(count($ratings) == 0){
            return 0;
        }
        $sum=array_sum($ratings);
        $rate=$sum / count($ratings);
        return floor($rate);
    }

    public function getCategoriesAttribute(){
        return $this->category()->get();
    }


    public function reviews(){
        return $this->hasMany('App\review');
    }

    public function getNotesAttribute(){
        return Note::where('status','!=',0)
                    ->where('invention_id',$this->id)
                    ->first();
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function registerMediaCollections(){
        $this->addMediaCollection('cac_certificate')
            ->acceptsMimeTypes(['image/jpeg','application/pdf','image/png','image/jpg']);
            
        $this->addMediaCollection('director_id')
            ->acceptsMimeTypes(['image/jpeg','application/pdf','image/png','image/jpg']);
            
            
        $this->addMediaCollection('business_plan')
            ->acceptsMimeTypes(['image/jpeg','application/pdf','image/png','image/jpg','application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document']);
            

        $this->addMediaCollection('other_document')
            ->acceptsMimeTypes(['image/jpeg','application/pdf','application/vnd.ms-powerpoint','application/vnd.openxmlformats-officedocument.presentationml.presentation','image/png','image/jpg','application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document']);
            
    }

    public function getCacCertificateAttribute(){
        $cac_cer = invention::find($this->id);
        return $cac_cer->getMedia('cac_certificate')->last();
    }
    public function getDirectorIdAttribute(){
        $director_id = invention::find($this->id);
        return $director_id->getMedia('director_id')->last();
    }
    public function getBusinessPlanAttribute(){
        $business_plan = invention::find($this->id);
        return $business_plan->getMedia('business_plan')->last();
    }
    public function getOtherDocumentAttribute(){
        $other_document = invention::find($this->id);
        return $other_document->getMedia('other_document')->last();
    }

    public function category(){
        return $this->belongsToMany('App\category','category_invention');
    }
}
