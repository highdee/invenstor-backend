<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\File;
use Spatie\MediaLibrary\Models\Media;

class invention extends Model implements HasMedia
{
    use HasMediaTrait;
    protected $guarded=[];

    protected $appends=['cac_certificate','director_id','business_plan','other_document','notes'];

    public function getNotesAttribute(){
        return Note::where('status','!=',0)
                    ->where('invention_id',$this->id)
                    ->first();
    }

    public function registerMediaCollections(){
        $this->addMediaCollection('cac_certificate')
            ->acceptsMimeTypes(['image/jpeg','application/pdf','image/png','image/jpg'])
            ->registerMediaConversions(function (Media $media){
                $this->addMediaConversion('card')
                    ->width(400)
                    ->height(300);
            });
        $this->addMediaCollection('director_id')
            ->acceptsMimeTypes(['image/jpeg','application/pdf','image/png','image/jpg'])
            ->registerMediaConversions(function (Media $media){
                $this->addMediaConversion('card')
                    ->width(400)
                    ->height(300);
            });
            
        $this->addMediaCollection('business_plan')
            ->acceptsMimeTypes(['image/jpeg','application/pdf','image/png','image/jpg','application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document'])
            ->registerMediaConversions(function (Media $media){
                $this->addMediaConversion('card')
                    ->width(400)
                    ->height(300);
            });

        $this->addMediaCollection('other_document')
            ->acceptsMimeTypes(['image/jpeg','application/pdf','application/vnd.ms-powerpoint','application/vnd.openxmlformats-officedocument.presentationml.presentation','image/png','image/jpg','application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document'])
            ->registerMediaConversions(function (Media $media){
                $this->addMediaConversion('card')
                    ->width(400)
                    ->height(300);
            });
    }

    public function getCacCertificateAttribute(){
        $cac_cer = invention::find($this->id);
        return $cac_cer->getMedia('cac_certificate')->last();
    }
    public function getDirectorIdAttribute(){
        $director_id = invention::find($this->id);
        return $director_id->getMedia('director_id')->last();
    }
    public function getBusinessPlanAttribute(){
        $business_plan = invention::find($this->id);
        return $business_plan->getMedia('business_plan')->last();
    }
    public function getOtherDocumentAttribute(){
        $other_document = invention::find($this->id);
        return $other_document->getMedia('other_document')->last();
    }
}
