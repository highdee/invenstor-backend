<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class review extends Model
{
    //

    protected  $appends=['user'];

    protected $casts=[
        'created_at'=>'datetime:D, d-M-Y'
    ];

    public function getUserAttribute(){
        return $this->user()->first();
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function getInventionIds(){
        return $this->inventions()->pluck('invention_id')->toArray();
    }
    public function inventions(){
        return $this->belongsTo('App\invention');
    }
}
