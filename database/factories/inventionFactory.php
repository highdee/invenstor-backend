<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\invention;
use Faker\Generator as Faker;

$factory->define(App\invention::class, function (Faker $faker) {
    return [
        'user_id'=>$faker->randomElement(\App\User::where('userType',0)->pluck('id')->toArray()),
        'status'=>$faker->numberBetween(3,3),
        'business_name'=>$faker->company(),
        'cac_no'=>$faker->numberBetween(),
        'email'=>$faker->name.'@gmail.com',
        'business_contact'=>$faker->phoneNumber,
        'year_reg'=>$faker->dateTimeBetween(date('d-m-y',strtotime('10-01-2018')),date('d-m-Y')),
        'industry'=>$faker->name,
        'business_address'=>$faker->address,
        'business_goal'=>$faker->sentence(1000),
        'business_vision'=>$faker->sentence(1000),
        'business_mission'=>$faker->sentence(1000),
        'description'=>$faker->paragraph(),
        'target_market'=>$faker->company,
        'business_stage'=>$faker->word,
        'no_shareholder'=>$faker->numberBetween(1,5),
        'no_employee'=>$faker->numberBetween(1,6),
        'value_of_business'=>$faker->randomFloat(0,1000,10000),
        'invest_required'=>$faker->randomFloat(0,1000,10000),
        'business_pitch'=>$faker->sentence(500),
        'equity'=>$faker->numberBetween(10,100),
        'reference_id'=>$faker->numberBetween(100000000,999999999999999),
    ];
});
