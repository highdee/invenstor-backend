<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_id');
            $table->string('status')->default('0');
            $table->string('business_name')->nullable();
            $table->string('cac_no')->nullable();
            $table->string('email')->nullable();
            $table->string('business_contact')->nullable();
            $table->string('year_reg')->nullable();
            $table->string('industry')->nullable();
            $table->string('business_address')->nullable();
            $table->text('business_goal')->nullable();
            $table->text('business_vision')->nullable();
            $table->text('business_mission')->nullable();
            $table->text('description')->nullable();
            $table->text('target_market')->nullable();
            $table->string('business_stage')->nullable();
            $table->string('no_shareholder')->nullable();
            $table->string('no_employee')->nullable();
            $table->string('value_of_business')->nullable();
            $table->string('invest_required')->nullable();
            $table->string('business_pitch')->nullable();
            $table->string('equity')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventions');
    }
}
