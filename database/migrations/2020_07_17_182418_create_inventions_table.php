<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_id');
            $table->string('status')->default('0');
            $table->string('business_name')->nullable();
            $table->string('cac_no')->nullable();
            $table->string('email')->nullable();
            $table->string('business_contact')->nullable();
            $table->date('year_reg')->nullable();
            $table->string('industry')->nullable();
            $table->string('business_address')->nullable();
            $table->longText('business_goal')->nullable();
            $table->longText('business_vision')->nullable();
            $table->longText('business_mission')->nullable();
            $table->longText('description')->nullable();
            $table->longText('target_market')->nullable();
            $table->string('business_stage')->nullable();
            $table->integer('no_shareholder')->nullable();
            $table->integer('no_employee')->nullable();
            $table->float('value_of_business')->nullable();
            $table->float('invest_required')->nullable();
            $table->longText('business_pitch')->nullable();
            $table->integer('equity')->nullable();
            $table->string('reference_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventions');
    }
}
