<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        $this->call(UsersTableSeeder::class);
        // $this->call(adminSeeder::class);
        // $this->call(inventionSeeder::class);
        $this->call(categorySeeder::class);
        // $this->call(category_invention_seeder::class);
    }
}
