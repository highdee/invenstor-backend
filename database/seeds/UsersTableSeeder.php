<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\User', 10)->create();

        DB::table('users')->insert([
            'name'=>'ilelaboye lekan',
            'phone'=>'0900909399',
            'email'=>'ilelaboyealekan@gmail.com',
            'userType'=>'0',
            'password'=>Hash::make(1234567)
        ]); 
    }
}
