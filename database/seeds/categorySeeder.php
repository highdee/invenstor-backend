<?php

use Illuminate\Database\Seeder;

class categorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name'=>'Agriculture'
        ]);
        DB::table('categories')->insert([
            'name'=>'Information Technology'
        ]);
        DB::table('categories')->insert([
            'name'=>'Financial'
        ]);
        DB::table('categories')->insert([
            'name'=>'Health care'
        ]);
        DB::table('categories')->insert([
            'name'=>'Education'
        ]);
        DB::table('categories')->insert([
            'name'=>'Forex'
        ]);
    }
}


