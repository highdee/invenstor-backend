<?php

use Illuminate\Database\Seeder;

class inventionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\invention::class,40)->create();
    }
}
