@extends('inc.admin_asset')
@section('adminHome')
	active
@endsection
@section('content')
	<div class="content-header row">
	</div>
	<div class="content-body">
		<!-- Dashboard Ecommerce Starts -->
		<section id="dashboard-ecommerce container-fluid">
			<div class="row">
				<div class="col-sm-4">
					<div class="card text-center">
						<div class="card-content">
							<div class="card-body py-1">
								<div class="badge-circle badge-circle-lg badge-circle-light-danger mx-auto mb-50">
									<i class="bx bx-user font-medium-5"></i>
								</div>
								<div class="text-muted line-ellipsis">Investor</div>
								<h3 class="mb-0">{{$investor}}</h3>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="card text-center">
						<div class="card-content">
							<div class="card-body py-1">
								<div class="badge-circle badge-circle-lg badge-circle-light-danger mx-auto mb-50">
									<i class="bx bx-user font-medium-5"></i>
								</div>
								<div class="text-muted line-ellipsis">Inventors</div>
								<h3 class="mb-0">{{$inventor}}</h3>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-12">
					<div class="card text-center">
						<div class="card-content">
							<div class="card-body py-1">
								<div class="badge-circle badge-circle-lg badge-circle-light-danger mx-auto mb-50">
									<i class="bx bx-user font-medium-5"></i>
								</div>
								<div class="text-muted line-ellipsis">Inventions</div>
								<h3 class="mb-0">{{$invention}}</h3>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
@endsection