@extends('inc.admin_asset')
@section('hoverToOpen')
	menu-collapsed
@endsection
@section('content')
    <div class="content-header row">
    </div>
    <div class="content-body">
        <section class="page-user-profile">
            <div class="row">
                <div class="col-12">
                    <div class="">
                        <div class="card-content">
                            <!-- user profile nav tabs start -->
                            <div class="card-body px-0">
                                <ul class="nav user-profile-nav justify-content-center justify-content-md-start nav-tabs border-bottom-0 mb-0" role="tablist">
                                    <li class="nav-item pb-0">
                                        <a class=" nav-link d-flex px-1 active" id="invention-tab" data-toggle="tab" href="#invention" aria-controls="invention" role="tab" aria-selected="true"><i class="bx bx-code"></i><span class="d-none d-md-block">Invention</span></a>
                                    </li>
                                    <li class="nav-item pb-0">
                                        <a class=" nav-link d-flex px-1" id="files-tab" data-toggle="tab" href="#files" aria-controls="files" role="tab" aria-selected="true"><i class="bx bx-file"></i><span class="d-none d-md-block">Files</span></a>
                                    </li>
                                    <li class="nav-item pb-0">
                                        <a class=" nav-link d-flex px-1" id="feed-tab" data-toggle="tab" href="#feed" aria-controls="feed" role="tab" aria-selected="true"><i class="bx bx-user"></i><span class="d-none d-md-block">Inventor</span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    @include('inc.notification_display')
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="tab-content">
                                <div class="tab-pane active" id="invention" aria-labelledby="invention-tab" role="tabpanel">
                                    <div class="card">
                                        <div class="card-content">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <h5 class="text-muted">Business Name</h5>
                                                        <h6 class="font-weight-light">{{$invention->business_name}}</h6>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <h5 class="text-muted">CAC Number</h5>
                                                        <h6 class="font-weight-light">{{$invention->cac_no}}</h6>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <h5 class="text-muted">Email</h5>
                                                        <h6 class="font-weight-light">{{$invention->email}}</h6>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <h5 class="text-muted">Value of Business</h5>
                                                        <h6 class="font-weight-light">${{$invention->value_of_business}}</h6>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <h5 class="text-muted">Investment Required</h5>
                                                        <h6 class="font-weight-light">${{$invention->invest_required}}</h6>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <h5 class="text-muted">Equity</h5>
                                                        <h6 class="font-weight-light">{{$invention->equity}}%</h6>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <h5 class="text-muted">Business Stage</h5>
                                                        <h6 class="font-weight-light">{{$invention->business_stage}}</h6>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <h5 class="text-muted">Number of Shareholder</h5>
                                                        <h6 class="font-weight-light">{{$invention->no_shareholder}}</h6>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <h5 class="text-muted">Number of Employee</h5>
                                                        <h6 class="font-weight-light">{{$invention->no_employee}}</h6>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <h5 class="text-muted">Business Contact</h5>
                                                        <h6 class="font-weight-light">{{$invention->business_contact}}</h6>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <h5 class="text-muted">Year of Registration</h5>
                                                        <h6 class="font-weight-light">{{$invention->year_reg}}</h6>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <h5 class="text-muted">Industry</h5>
                                                        <h6 class="font-weight-light">{{$invention->industry}}</h6>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <h5 class="text-muted">Target Market</h5>
                                                        <h6 class="font-weight-light">{{$invention->target_market}}</h6>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <h5 class="text-muted">Business Address</h5>
                                                        <h6 class="font-weight-light">{{$invention->business_address}}</h6>
                                                    </div>
                                                    <div class="col-12">
                                                        <h5 class="text-muted">Description</h5>
                                                        <h6 class="font-weight-light">{{$invention->description}}</h6>
                                                    </div>
                                                    <div class="col-12">
                                                        <h5 class="text-muted">Business Goal</h5>
                                                        <h6 class="font-weight-light">{{$invention->business_goal}}</h6>
                                                    </div>
                                                    <div class="col-12">
                                                        <h5 class="text-muted">Business Vision</h5>
                                                        <h6 class="font-weight-light">{{$invention->business_vision}}</h6>
                                                    </div>
                                                    <div class="col-12">
                                                        <h5 class="text-muted">Business Mission</h5>
                                                        <h6 class="font-weight-light">{{$invention->business_mission}}</h6>
                                                    </div>
                                                </div>
                                                <div class="mt-1">
                                                    @if ($invention->status != 0)
                                                        <button class="btn btn-warning mr-1" data-toggle="modal" data-target="#cancel">Cancel Invention</button>
                                                        <button class="btn btn-danger mr-1" data-toggle="modal" data-target="#default">Reject Invention</button>
                                                        <button class="btn btn-success mr-1" data-toggle="modal" data-target="#rate">Accept Invention</button>
                                                    @endif
                                                </div>
                                                <div class="row">
                                                    <div class="modal fade text-left" id="cancel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
                                                            <div class="modal-content" >
                                                                <div class="modal-header">
                                                                    <h3 class="modal-title" id="myModalLabel1">Cancel Invention</h3>
                                                                    <button type="button" class="close rounded-pill" data-dismiss="modal" aria-label="Close">
                                                                        <i class="bx bx-x"></i>
                                                                    </button>
                                                                </div>
                                                                
                                                                <div class="modal-body">
                                                                    <section id="multiple-column-form">
                                                                        <div class="row match-height">
                                                                            <div class="col-12">
                                                                                <div class="card-content">
                                                                                    <div class="card-body p-0">
                                                                                        <form class="form" method="post" action="/admin/cancelInvention">
                                                                                            @csrf
                                                                                            <div class="form-body">
                                                                                                <div class="row">
                                                                                                    <input type="hidden" name="inventionId" value="{{$invention->id}}">
                                                                                                    <div class="col-12">
                                                                                                        <label for="note">Reason for cancelling invention</label>
                                                                                                        <div class="form-label-group">
                                                                                                            <fieldset class="form-group">
                                                                                                                <textarea class="form-control" name="note" id="note" rows="5" placeholder="Reasons for cancelling"></textarea>
                                                                                                            </fieldset>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="col-12 d-flex justify-content-end">
                                                                                                        <button type="submit" class="btn btn-primary btn-block mb-1 mt-0">Cancel Invention</button>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </section>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
                                                            <div class="modal-content" >
                                                                <div class="modal-header">
                                                                    <h3 class="modal-title" id="myModalLabel1">Reject Invention</h3>
                                                                    <button type="button" class="close rounded-pill" data-dismiss="modal" aria-label="Close">
                                                                        <i class="bx bx-x"></i>
                                                                    </button>
                                                                </div>
                                                                
                                                                <div class="modal-body">
                                                                    <section id="multiple-column-form">
                                                                        <div class="row match-height">
                                                                            <div class="col-12">
                                                                                <div class="card-content">
                                                                                    <div class="card-body p-0">
                                                                                        <form class="form" method="post" action="/admin/rejectInvention">
                                                                                            @csrf
                                                                                            <div class="form-body">
                                                                                                <div class="row">
                                                                                                    <input type="hidden" name="inventionId" value="{{$invention->id}}">
                                                                                                    <div class="col-12">
                                                                                                        <label for="note">Reason for rejecting invention</label>
                                                                                                        <div class="form-label-group">
                                                                                                            <fieldset class="form-group">
                                                                                                                <textarea class="form-control" name="note" id="note" rows="5" placeholder="Reasons for rejection"></textarea>
                                                                                                            </fieldset>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="col-12 d-flex justify-content-end">
                                                                                                        <button type="submit" class="btn btn-primary btn-block mb-1 mt-0">Reject Invention</button>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </section>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="modal fade text-left" id="rate" tabindex="-1" role="dialog" aria-labelledby="rate" aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable" role="document">
                                                            <div class="modal-content" >
                                                                <div class="modal-header px-1">
                                                                    <h3 class="modal-title" id="myModalLabel1">Rate Invention</h3>
                                                                    <button type="button" class="close rounded-pill" data-dismiss="modal" aria-label="Close">
                                                                        <i class="bx bx-x"></i>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body px-1 pb-0">
                                                                    <section id="multiple-column-form">
                                                                        <div class="row match-height">
                                                                            <div class="col-12">
                                                                                <div class="card-content">
                                                                                    <div class="card-body p-0">
                                                                                        <form class="form" method="post" action="/admin/acceptInvention">
                                                                                            @csrf
                                                                                            <div class="form-body">
                                                                                                <div class="row">
                                                                                                    <input type="hidden" name="inventionId" value="{{$invention->id}}">
                                                                                                    <div class="col-12">
                                                                                                        <label for="note">Review invention</label>
                                                                                                        <div class="form-label-group">
                                                                                                            <fieldset class="form-group">
                                                                                                                <textarea class="form-control" name="review" id="note" rows="4" placeholder="Invention review"></textarea>
                                                                                                            </fieldset>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <input type="hidden" value="" name="rating" id="rating">
                                                                                            <div class="form-group">
                                                                                                <div class="rating cursor-pointer">
                                                                                                    <i class="bx bxs-star" onclick="rate(1)"></i>
                                                                                                    <i class="bx bxs-star" onclick="rate(2)"></i>
                                                                                                    <i class="bx bxs-star" onclick="rate(3)"></i>
                                                                                                    <i class="bx bxs-star" onclick="rate(4)"></i>
                                                                                                    <i class="bx bxs-star" onclick="rate(5)"></i>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row">
                                                                                                <div class="col-12 d-flex justify-content-end">
                                                                                                    <button type="submit" class="btn btn-success btn-block mt-0">Accept Invention</button>
                                                                                                </div>

                                                                                            </div>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </section>
                                                                </div>
                                                                {{-- <div class="modal-body">
                                                                    <section id="multiple-column-form">
                                                                        <div class="row match-height">
                                                                            <div class="col-12">
                                                                                <div class="card-content rate-invention">
                                                                                    <div class="card-body d-flex align-item-center justify-content-center rating cursor-pointer">
                                                                                        <i class="bx bxs-star" onclick="rate(1)"></i>
                                                                                        <i class="bx bxs-star" onclick="rate(2)"></i>
                                                                                        <i class="bx bxs-star" onclick="rate(3)"></i>
                                                                                        <i class="bx bxs-star" onclick="rate(4)"></i>
                                                                                        <i class="bx bxs-star" onclick="rate(5)"></i>
                                                                                    </div>
                                                                                    <form action="/admin/acceptInvention" method="post" class="text-center">
                                                                                        @csrf
                                                                                        <input type="hidden" value="{{$invention->id}}" name="inventionId">
                                                                                        <input type="hidden" value="" name="rating" id="rating">
                                                                                        <button type="submit" class="btn btn-success">Accept Invention</button>
                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </section>
                                                                </div> --}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="tab-pane container-fluid" id="files" aria-labelledby="files-tab" role="tabpanel">
                                    <div class="row">
                                        @if (count($invention->getMedia('cac_certificate')) > 0)
                                            <div class="col-md-6">
                                                <div class="card">
                                                    <a href="{{$invention->getMedia('cac_certificate')->last()->getUrl()}}" target="_blank">
                                                        <div class="card-content">
                                                            @if (explode('/',$invention->getMedia('cac_certificate')->last()->mime_type)[0] == 'image')
                                                                <div class="img-res" style="background-image: url({{$invention->getMedia('cac_certificate')->last()->getUrl()}});">
                            
                                                                </div>
                                                                {{-- <img src="{{$invention->getMedia('cac_certificate')->last()->getUrl()}}" class="card-img-top img-fluid" alt="singleminded"> --}}
                                                            @endif
                                                        </div>
                                                        <div class="card-footer d-flex justify-content-between">
                                                            <span>CAC Certificate</span>
                                                            <a href="/download/cac_certificate/{{$invention->id}}" class="btn btn-light-primary"><i class="bx bxs-download"></i></a>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                    
                                        @if (count($invention->getMedia('director_id')) > 0)
                                            <div class="col-md-6">
                                                <div class="card">
                                                    <a href="{{$invention->getMedia('director_id')->last()->getUrl()}}" target="_blank">
                                                        <div class="card-content">
                                                            @if (explode('/',$invention->getMedia('director_id')->last()->mime_type)[0] == 'image')
                                                                <div style="background-image: url({{$invention->getMedia('director_id')->last()->getUrl()}});">
                            
                                                                </div>
                                                                {{-- <img src="{{$invention->getMedia('director_id')->last()->getUrl()}}" class="card-img-top img-fluid" alt="singleminded"> --}}
                                                            @endif
                                                        </div>
                                                        <div class="card-footer d-flex justify-content-between">
                                                            <span>Director Id</span>
                                                            <a href="/download/director_id/{{$invention->id}}" class="btn btn-light-primary"><i class="bx bxs-download"></i></a>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (count($invention->getMedia('business_plan')) > 0)    
                                            <div class="col-md-6">
                                                <div class="card">
                                                    <a href="{{$invention->getMedia('business_plan')->last()->getUrl()}}" target="_blank">
                                                        <div class="card-content">
                                                            @if (explode('/',$invention->getMedia('business_plan')->last()->mime_type)[0] == 'image')
                                                                <div class="img-res" style="background-image: url({{$invention->getMedia('business_plan')->last()->getUrl()}});">
                            
                                                                </div>
                                                                {{-- <img src="{{$invention->getMedia('business_plan')->last()->getUrl()}}" class="card-img-top img-fluid" alt="singleminded"> --}}
                                                            @endif
                                                        </div>
                                                        <div class="card-footer d-flex justify-content-between">
                                                            <span>Business Plan</span>
                                                            <a href="/download/business_plan/{{$invention->id}}" class="btn btn-light-primary"><i class="bx bxs-download"></i></a>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (count($invention->getMedia('other_document')) > 0)
                                            <div class="col-md-6">
                                                <div class="card">
                                                    <a href="{{$invention->getMedia('other_document')->last()->getUrl()}}" target="_blank">
                                                        <div class="card-content">
                                                            @if (explode('/',$invention->getMedia('other_document')->last()->mime_type)[0] == 'image')
                                                                <div class="img-res" style="background-image: url({{$invention->getMedia('other_document')->last()->getUrl()}});">
                            
                                                                </div>
                                                                {{-- <img src="{{$invention->getMedia('other_document')->last()->getUrl()}}" class="card-img-top img-fluid" alt="singleminded"> --}}
                                                            @endif
                                                        </div>
                                                        <div class="card-footer d-flex justify-content-between">
                                                            <span>Other Document</span>
                                                            <a href="/download/other_document/{{$invention->id}}" class="btn btn-light-primary"><i class="bx bxs-download"></i></a>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="tab-pane" id="feed" aria-labelledby="feed-tab" role="tabpanel">
                                    <div class="card">
                                        <div class="card-content">
                                            <div class="card-body">
                                                <h4 class="card-title">Inventor details</h4>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h4><small class="text-muted">Name</small></h4>
                                                        <h6 class="font-weight-light">{{$invention->user->firstname .' '. $invention->user->lastname}}</h6>
                                                    </div>
                                                    <div class="col-12">
                                                        <h4><small class="text-muted">Phone</small></h4>
                                                        <h6 class="font-weight-light">{{$invention->user->phone}}</h6>
                                                    </div>
                                                    <div class="col-12">
                                                        <h4><small class="text-muted">Email</small></h4>
                                                        <h6 class="font-weight-light">{{$invention->user->email}}</h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
<script>
    function rate(number){
        var stars=document.getElementsByClassName('bxs-star');
        for (let i = 0; i < number; i++) {
            stars[i].style.color='red';
        }
        document.getElementById('rating').value = number;
    }
</script>