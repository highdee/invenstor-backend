@extends('inc.admin_asset')
@section('invention')
	active
@endsection
@section('content')
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1 mb-0">Users</h5>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item"><a href="/admin"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active">Inventions List
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        @include('inc.notification_display')  
        <section id="basic-datatable">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body card-dashboard">
                                <div class="table-responsive">
                                    <table class="table zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>Business Name</th>
                                                <th>CAC number</th>
                                                <th>Industry</th>
                                                <th>Value of Business</th>
                                                <th>Business Stage</th>
                                                <th>Target Market</th>
                                                <th>Status</th>
                                            </tr>  
                                        </thead>
                                        <tbody class="table-hover">
                                            @foreach($inventions as $inv)
                                                <tr onclick="window.location.href='/admin/invention/{{$inv->id}}'">                                
                                                    <td>{{$inv->business_name}}</td>
                                                    <td>{{$inv->cac_no}}</td>
                                                    <td>{{$inv->industry}}</td>
                                                    <td>{{$inv->value_of_business}}</td> 
                                                    <td>{{$inv->business_stage}}</td> 
                                                    <td>{{$inv->target_market}}</td> 
                                                    <td>
                                                        @if ($inv->status == 1)
                                                        <span class="text-secondary"> Under Review</span>
                                                        @elseif($inv->status == -1)
                                                        <span class="text-danger"> Rejected</span>
                                                        @elseif($inv->status == 2)
                                                        <span class="text-primary"> Cancelled</span>
                                                        @elseif($inv->status == 3)
                                                            <span class="text-success">Accepted</span>
                                                        @endif    
                                                    </td> 
                                                </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Business Name</th>
                                                <th>CAC number</th>
                                                <th>Industry</th>
                                                <th>Value of Business</th>
                                                <th>Business Stage</th>
                                                <th>Target Market</th>
                                                <th>Status</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection