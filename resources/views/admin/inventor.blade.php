@extends('inc.admin_asset')
@section('inventor')
	active
@endsection
@section('content')
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1 mb-0">Users</h5>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item"><a href="/admin"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active">Inventors List
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section id="basic-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table zero-configuration">
                                    <thead>
                                        <tr>
                                            <th>First name</th>
                                            <th>Last anme</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                        </tr>  
                                    </thead>
                                    <tbody class="table-hover">
                                        @foreach($inventors as $inv)
                                            <tr onclick="window.location.href='/admin/inventor_show/{{$inv->id}}'">                                
                                                <td>{{$inv->firstname}}</td>
                                                <td>{{$inv->lastname}}</td>
                                                <td>{{$inv->phone}}</td>
                                                <td>{{$inv->email}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>First name</th>
                                            <th>Last anme</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection