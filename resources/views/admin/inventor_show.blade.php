@extends('inc.admin_asset')
@section('hoverToOpen')
	menu-collapsed
@endsection
@section('content')
    <div class="content-header row">
    </div>
    <div class="content-body">
        <!-- page user profile start -->
        <section class="page-user-profile">
            <div class="row">
                <div class="col-12">
                    <!-- user profile heading section start -->
                    <div class="">
                        <div class="card-content">
                            <!-- user profile nav tabs start -->
                            <div class="card-body px-0">
                                <ul class="nav user-profile-nav justify-content-center justify-content-md-start nav-tabs border-bottom-0 mb-0" role="tablist">
                                    <li class="nav-item pb-0">
                                        <a class=" nav-link d-flex px-1 active" id="feed-tab" data-toggle="tab" href="#feed" aria-controls="feed" role="tab" aria-selected="true"><i class="bx bx-home"></i><span class="d-none d-md-block">User</span></a>
                                    </li>
                                    <li class="nav-item pb-0">
                                        <a class=" nav-link d-flex px-1" id="invention-tab" data-toggle="tab" href="#invention" aria-controls="invention" role="tab" aria-selected="true"><i class="bx bx-code"></i><span class="d-none d-md-block">Invention</span></a>
                                    </li>
                                    <li class="nav-item pb-0">
                                        <a class=" nav-link d-flex px-1" id="files-tab" data-toggle="tab" href="#files" aria-controls="files" role="tab" aria-selected="true"><i class="bx bx-file"></i><span class="d-none d-md-block">Files</span></a>
                                    </li>
                                </ul>
                            </div>
                            <!-- user profile nav tabs ends -->
                        </div>
                    </div>
                    <!-- user profile heading section ends -->

                    <!-- user profile content section start -->
                    @include('inc.notification_display')
                    <div class="row">
                        <!-- user profile nav tabs content start -->
                        <div class="col-lg-12">
                            <div class="tab-content">
                                <div class="tab-pane active" id="feed" aria-labelledby="feed-tab" role="tabpanel">
                                    
                                    <div class="card">
                                        <div class="card-content">
                                            <div class="card-body">
                                                <h5 class="card-title">Basic details</h5>
                                                <ul class="list-unstyled">
                                                    <h4><li><i class="bx bx-user mr-50 mb-1"></i>{{$invent->name}}</li></h4>
                                                    {{-- <h4><li><i class="cursor-pointer bx bx-map mb-1 mr-50"></i>{{$salon->address}}</li></h4>
                                                    <h4><li><i class="cursor-pointer bx bx-map mb-1 mr-50"></i>{{$salon->city}}</li></h4> --}}
                                                    <h4><li><i class="cursor-pointer bx bx-phone-call mb-1 mr-50"></i>{{$invent->phone}} </li></h4>
                                                    <h4><li><i class="cursor-pointer bx bx-envelope mb-1 mr-50"></i>{{$invent->email}}</li></h4>
                                                </ul>
                                                <div>
                                                    <form action="/admin/banUser" method="Post">
                                                        @csrf
                                                        <input type="hidden" value="{{$invent->id}}" name="user_id">
                                                        <input type="submit" class="btn {{ $invent->status == 0 ? 'btn-success' : 'btn-danger'}}" value="{{ $invent->status == 0 ? 'active' : 'banned'}}" >
                                                        
                                                        <a href="/admin/deleteUser/{{$invent->id}}" class="btn btn-danger">Delete</a>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane container-fluid" id="invention" aria-labelledby="invention-tab" role="tabpanel">
                                    
                                    <div class="card">
                                        <div class="card-content">
                                            <div class="card-body">
                                                
                                                @if (empty($invent->inventor))
                                                    <p class="alert alert-primary  text-center">No invention yet</p>
                                                @else
                                                    
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <h5>Business Name</h5>
                                                            <p>{{$invent->inventor->business_name}}</p>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <h5>CAC Number</h5>
                                                            <p>{{$invent->inventor->cac_no}}</p>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <h5>Email</h5>
                                                            <p>{{$invent->inventor->email}}</p>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <h5>Value of Business</h5>
                                                            <p>{{$invent->inventor->value_of_business}}</p>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <h5>Investment Required</h5>
                                                            <p>{{$invent->inventor->invest_required}}</p>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <h5>Equity</h5>
                                                            <p>{{$invent->inventor->equity}}</p>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <h5>Business Stage</h5>
                                                            <p>{{$invent->inventor->business_stage}}</p>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <h5>Number of Shareholder</h5>
                                                            <p>{{$invent->inventor->no_shareholder}}</p>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <h5>Number of Employee</h5>
                                                            <p>{{$invent->inventor->no_employee}}</p>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <h5>Business Contact</h5>
                                                            <p>{{$invent->inventor->business_contact}}</p>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <h5>Year of Registration</h5>
                                                            <p>{{$invent->inventor->year_reg}}</p>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <h5>Industry</h5>
                                                            <p>{{$invent->inventor->industry}}</p>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <h5>Target Market</h5>
                                                            <p>{{$invent->inventor->target_market}}</p>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <h5>Business Address</h5>
                                                            <p>{{$invent->inventor->business_address}}</p>
                                                        </div>
                                                        <div class="col-12">
                                                            <h5>Description</h5>
                                                            <p>{{$invent->inventor->description}}</p>
                                                        </div>
                                                        <div class="col-12">
                                                            <h5>Business Goal</h5>
                                                            <p>{{$invent->inventor->business_goal}}</p>
                                                        </div>
                                                        <div class="col-12">
                                                            <h5>Business Vision</h5>
                                                            <p>{{$invent->inventor->business_vision}}</p>
                                                        </div>
                                                        <div class="col-12">
                                                            <h5>Business Mission</h5>
                                                            <p>{{$invent->inventor->business_mission}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="mt-1">
                                                        @if ($invent->inventor->status != 0)
                                                            <button class="btn btn-warning mr-1" data-toggle="modal" data-target="#cancel">Cancel Invention</button>
                                                            <button class="btn btn-danger mr-1" data-toggle="modal" data-target="#default">Reject Invention</button>
                                                            <form action="/admin/acceptInvention" method="post" class="d-inline">
                                                                @csrf
                                                                <input type="hidden" value="{{$invent->inventor->id}}" name="inventionId">
                                                                <button type="submit" class="btn btn-success">Accept Invention</button>
                                                            </form>
                                                        @endif
                                                    </div>
                                                    <div class="row"  >
                                                        <div class="modal fade text-left" id="cancel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                                            <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
                                                                <div class="modal-content" >
                                                                    <div class="modal-header">
                                                                        <h3 class="modal-title" id="myModalLabel1">Cancel Invention</h3>
                                                                        <button type="button" class="close rounded-pill" data-dismiss="modal" aria-label="Close">
                                                                            <i class="bx bx-x"></i>
                                                                        </button>
                                                                    </div>
                                                                    
                                                                    <div class="modal-body">
                                                                        <section id="multiple-column-form">
                                                                            <div class="row match-height">
                                                                                <div class="col-12">
                                                                                    <div class="card-content">
                                                                                        <div class="card-body p-0">
                                                                                            <form class="form" method="post" action="/admin/cancelInvention">
                                                                                                @csrf
                                                                                                <div class="form-body">
                                                                                                    <div class="row">
                                                                                                        <input type="hidden" name="inventionId" value="{{$invent->inventor->id}}">
                                                                                                        <div class="col-12">
                                                                                                            <label for="note">Reason for cancelling invention</label>
                                                                                                            <div class="form-label-group">
                                                                                                                <fieldset class="form-group">
                                                                                                                    <textarea class="form-control" name="note" id="note" rows="5" placeholder="Reasons for cancelling"></textarea>
                                                                                                                </fieldset>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="col-12 d-flex justify-content-end">
                                                                                                            <button type="submit" class="btn btn-primary btn-block mb-1 mt-0">Cancel Invention</button>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </form>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </section>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                                            <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
                                                                <div class="modal-content" >
                                                                    <div class="modal-header">
                                                                        <h3 class="modal-title" id="myModalLabel1">Reject Invention</h3>
                                                                        <button type="button" class="close rounded-pill" data-dismiss="modal" aria-label="Close">
                                                                            <i class="bx bx-x"></i>
                                                                        </button>
                                                                    </div>
                                                                    
                                                                    <div class="modal-body">
                                                                        <section id="multiple-column-form">
                                                                            <div class="row match-height">
                                                                                <div class="col-12">
                                                                                    <div class="card-content">
                                                                                        <div class="card-body p-0">
                                                                                            <form class="form" method="post" action="/admin/rejectInvention">
                                                                                                @csrf
                                                                                                <div class="form-body">
                                                                                                    <div class="row">
                                                                                                        <input type="hidden" name="inventionId" value="{{$invent->inventor->id}}">
                                                                                                        <div class="col-12">
                                                                                                            <label for="note">Reason for rejecting invention</label>
                                                                                                            <div class="form-label-group">
                                                                                                                <fieldset class="form-group">
                                                                                                                    <textarea class="form-control" name="note" id="note" rows="5" placeholder="Reasons for rejection"></textarea>
                                                                                                                </fieldset>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="col-12 d-flex justify-content-end">
                                                                                                            <button type="submit" class="btn btn-primary btn-block mb-1 mt-0">Reject Invention</button>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </form>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </section>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane container-fluid" id="files" aria-labelledby="files-tab" role="tabpanel">
                                    <div class="row">
                                        <div class="col-md-6">
                                            @if (count($invent->inventor->getMedia('cac_certificate')) > 0)
                                                <div class="card">
                                                    <a href="{{$invent->inventor->getMedia('cac_certificate')->last()->getUrl()}}" target="_blank">
                                                        <div class="card-content">
                                                            @if (explode('/',$invent->inventor->getMedia('cac_certificate')->last()->mime_type)[0] == 'image')
                                                                <img src="{{$invent->inventor->getMedia('cac_certificate')->last()->getUrl('card')}}" class="card-img-top img-fluid" alt="singleminded">
                                                            @endif
                                                        </div>
                                                        <div class="card-footer d-flex justify-content-between">
                                                            <span>CAC Certificate</span>
                                                            <a href="/download/cac_certificate/{{$invent->inventor->id}}" class="btn btn-light-primary"><i class="bx bxs-download"></i></a>
                                                        </div>
                                                    </a>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="col-md-6">
                                            @if (count($invent->inventor->getMedia('director_id')) > 0)
                                                <div class="card">
                                                    <a href="{{$invent->inventor->getMedia('director_id')->last()->getUrl()}}" target="_blank">
                                                        <div class="card-content">
                                                            @if (explode('/',$invent->inventor->getMedia('director_id')->last()->mime_type)[0] == 'image')
                                                                <img src="{{$invent->inventor->getMedia('director_id')->last()->getUrl('card')}}" class="card-img-top img-fluid" alt="singleminded">
                                                            @endif
                                                        </div>
                                                        <div class="card-footer d-flex justify-content-between">
                                                            <span>Director Id</span>
                                                            <a href="/download/director_id/{{$invent->inventor->id}}" class="btn btn-light-primary"><i class="bx bxs-download"></i></a>
                                                        </div>
                                                    </a>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="col-md-6">
                                            @if (count($invent->inventor->getMedia('business_plan')) > 0)
                                                <div class="card">
                                                    <a href="{{$invent->inventor->getMedia('business_plan')->last()->getUrl()}}" target="_blank">
                                                        <div class="card-content">
                                                            @if (explode('/',$invent->inventor->getMedia('business_plan')->last()->mime_type)[0] == 'image')
                                                                <img src="{{$invent->inventor->getMedia('business_plan')->last()->getUrl('card')}}" class="card-img-top img-fluid" alt="singleminded">
                                                            @endif
                                                        </div>
                                                        <div class="card-footer d-flex justify-content-between">
                                                            <span>Business Plan</span>
                                                            <a href="/download/business_plan/{{$invent->inventor->id}}" class="btn btn-light-primary"><i class="bx bxs-download"></i></a>
                                                        </div>
                                                    </a>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="col-md-6">
                                            @if (count($invent->inventor->getMedia('other_document')) > 0)
                                                <div class="card">
                                                    <a href="{{$invent->inventor->getMedia('other_document')->last()->getUrl()}}" target="_blank">
                                                        <div class="card-content">
                                                            @if (explode('/',$invent->inventor->getMedia('other_document')->last()->mime_type)[0] == 'image')
                                                                <img src="{{$invent->inventor->getMedia('other_document')->last()->getUrl('card')}}" class="card-img-top img-fluid" alt="singleminded">
                                                            @endif
                                                        </div>
                                                        <div class="card-footer d-flex justify-content-between">
                                                            <span>Other Document</span>
                                                            <a href="/download/other_document/{{$invent->inventor->id}}" class="btn btn-light-primary"><i class="bx bxs-download"></i></a>
                                                        </div>
                                                    </a>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection