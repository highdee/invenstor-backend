@extends('inc.admin_asset')
@section('hoverToOpen')
	menu-collapsed
@endsection
@section('content')
    <div class="content-header row">
    </div>
    <div class="content-body">
        <section class="page-user-profile">
            <div class="row">
                <div class="col-12">
                    <div class="">
                        <div class="card-content">
                            <div class="card-body px-0">
                                <ul class="nav user-profile-nav justify-content-center justify-content-md-start nav-tabs border-bottom-0 mb-0" role="tablist">
                                    <li class="nav-item pb-0">
                                        <a class=" nav-link d-flex px-1 active" id="feed-tab" data-toggle="tab" href="#feed" aria-controls="feed" role="tab" aria-selected="true"><i class="bx bx-home"></i><span class="d-none d-md-block">User</span></a>
                                    </li>
                                    <li class="nav-item pb-0">
                                        <a class=" nav-link d-flex px-1" id="invention-tab" data-toggle="tab" href="#invention" aria-controls="invention" role="tab" aria-selected="true"><i class="bx bx-code"></i><span class="d-none d-md-block">Invention</span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    @include('inc.notification_display')
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="tab-content">
                                <div class="tab-pane active" id="feed" aria-labelledby="feed-tab" role="tabpanel">
                                    <div class="card">
                                        <div class="card-content">
                                            <div class="card-body">
                                                <h4 class="card-title">Inventor details</h4>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h4><small class="text-muted">Name</small></h4>
                                                        <h6 class="font-weight-light">{{$invent->firstname.' '.$invent->lastname}}</h6>
                                                    </div>
                                                    <div class="col-12">
                                                        <h4><small class="text-muted">Phone</small></h4>
                                                        <h6 class="font-weight-light">{{$invent->phone}}</h6>
                                                    </div>
                                                    <div class="col-12">
                                                        <h4><small class="text-muted">Email</small></h4>
                                                        <h6 class="font-weight-light">{{$invent->email}}</h6>
                                                    </div>
                                                </div>
                                                <div class="mt-2">
                                                    <form action="/admin/banUser" method="Post">
                                                        @csrf
                                                        <input type="hidden" value="{{$invent->id}}" name="user_id">
                                                        <input type="submit" class="btn {{ $invent->status == 0 ? 'btn-success' : 'btn-danger'}}" value="{{ $invent->status == 0 ? 'active' : 'banned'}}" >
                                                        
                                                        <a href="/admin/deleteUser/{{$invent->id}}" class="btn btn-danger">Delete</a>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane container-fluid" id="invention" aria-labelledby="invention-tab" role="tabpanel">
                                    @if (count($invent->inventions) < 1)
                                        <p class="alert alert-primary  text-center">No invention yet</p>
                                    @else
                                        <div class="row">
                                            @foreach ($invent->inventions as $invention)
                                                <div class="col-sm-4">
                                                    <a href="/admin/invention/{{$invention->id}}">
                                                        <div class="card">
                                                            <div class="card-content">
                                                                <div class="card-body invent pb-0">
                                                                    <h6>Business Name</h6>
                                                                    <p>{{$invention->business_name}}</p>
                                                                    <h6>Pitch</h6>
                                                                    <a href="{{$invention->business_pitch}}">{{$invention->business_pitch}}</a>
                                                                    <h6>Industry</h6>
                                                                    <p>{{$invention->industry}}</p>
                                                                    <h6>Value of Business</h6>
                                                                    <p>${{$invention->value_of_business}}</p>
                                                                    <h6>Invest required</h6>
                                                                    <p>${{$invention->invest_required}}</p>
                                                                    <h6>Equity</h6>
                                                                    <p class="">{{$invention->equity}}%</p>
                                                                    <div class="rating mb-1">
                                                                        @if ($invention->status == 3)
                                                                            @for ($i = 0; $i < $invention->rating; $i++)
                                                                                <i class="bx bxs-star text-danger"></i>
                                                                            @endfor
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <div class="card-footer pt-0 d-flex justify-content-between">
                                                                    @if ($invention->status == 0)
                                                                        <button class="btn btn-warning">Incomplete</button>
                                                                    @elseif($invention->status == 1)
                                                                        <button class="btn btn-warning">Review</button>
                                                                    @elseif($invention->status == 2)
                                                                        <button class="btn btn-danger">Cancelled</button>
                                                                    @elseif($invention->status == 3)
                                                                        <button class="btn btn-success">Accepted</button>
                                                                    @elseif($invention->status == -1)
                                                                        <button class="btn btn-danger">Rejected</button>
                                                                    @endif
                                                                    <a href="/admin/invention/{{$invention->id}}" class="btn btn-light-primary">Read More</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
<script>
    function rate(number){
        var stars=document.getElementsByClassName('bxs-star');
        for (let i = 0; i < number; i++) {
            stars[i].style.color='red';
        }
        document.getElementById('rating').value = number;
    }
</script>