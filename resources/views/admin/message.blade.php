@if(count($errors) > 0)
	@foreach($errors->all() as $error)
		<div class="alert alert-danger alert-dismissible mb-2" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="d-flex align-items-center">
                <i class="bx bx-error"></i>
                <span>
                    {{ $error }}
                </span>
            </div>
        </div>
	@endforeach
@endif

@if(session('successMsg'))
    <div class="alert alert-success alert-dismissible mb-2" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="d-flex align-items-center">
            <i class="bx bx-like"></i>
            <span>
                {{ session('successMsg') }}
            </span>
        </div>
    </div>
@endif
@if(session('errorMsg'))
    <div class="alert alert-danger alert-dismissible mb-2" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="d-flex align-items-center">
            <i class="bx bx-error"></i>
            <span>
               {{ session('errorMsg') }}
            </span>
        </div>
    </div>
@endif





<div class="tab-pane container-fluid" id="files" aria-labelledby="files-tab" role="tabpanel">
    <div class="row">
        @if (!is_null($invent->inventions))
            <div class="col-md-6">
                @if (count($invent->inventions->getMedia('cac_certificate')) > 0)
                    <div class="card">
                        <a href="{{$invent->inventions->getMedia('cac_certificate')->last()->getUrl()}}" target="_blank">
                            <div class="card-content">
                                @if (explode('/',$invent->inventions->getMedia('cac_certificate')->last()->mime_type)[0] == 'image')
                                    <div class="img-res" style="background-image: url({{$invent->inventions->getMedia('cac_certificate')->last()->getUrl()}});">

                                    </div>
                                    {{-- <img src="{{$invent->inventions->getMedia('cac_certificate')->last()->getUrl()}}" class="card-img-top img-fluid" alt="singleminded"> --}}
                                @endif
                            </div>
                            <div class="card-footer d-flex justify-content-between">
                                <span>CAC Certificate</span>
                                <a href="/download/cac_certificate/{{$invent->inventions->id}}" class="btn btn-light-primary"><i class="bx bxs-download"></i></a>
                            </div>
                        </a>
                    </div>
                @endif
            </div>
            <div class="col-md-6">
                @if (count($invent->inventions->getMedia('director_id')) > 0)
                    <div class="card">
                        <a href="{{$invent->inventions->getMedia('director_id')->last()->getUrl()}}" target="_blank">
                            <div class="card-content">
                                @if (explode('/',$invent->inventions->getMedia('director_id')->last()->mime_type)[0] == 'image')
                                    <div style="background-image: url({{$invent->inventions->getMedia('director_id')->last()->getUrl()}});">

                                    </div>
                                    {{-- <img src="{{$invent->inventions->getMedia('director_id')->last()->getUrl()}}" class="card-img-top img-fluid" alt="singleminded"> --}}
                                @endif
                            </div>
                            <div class="card-footer d-flex justify-content-between">
                                <span>Director Id</span>
                                <a href="/download/director_id/{{$invent->inventions->id}}" class="btn btn-light-primary"><i class="bx bxs-download"></i></a>
                            </div>
                        </a>
                    </div>
                @endif
            </div>
            <div class="col-md-6">
                @if (count($invent->inventions->getMedia('business_plan')) > 0)
                    <div class="card">
                        <a href="{{$invent->inventions->getMedia('business_plan')->last()->getUrl()}}" target="_blank">
                            <div class="card-content">
                                @if (explode('/',$invent->inventions->getMedia('business_plan')->last()->mime_type)[0] == 'image')
                                    <div class="img-res" style="background-image: url({{$invent->inventions->getMedia('business_plan')->last()->getUrl()}});">

                                    </div>
                                    {{-- <img src="{{$invent->inventions->getMedia('business_plan')->last()->getUrl()}}" class="card-img-top img-fluid" alt="singleminded"> --}}
                                @endif
                            </div>
                            <div class="card-footer d-flex justify-content-between">
                                <span>Business Plan</span>
                                <a href="/download/business_plan/{{$invent->inventions->id}}" class="btn btn-light-primary"><i class="bx bxs-download"></i></a>
                            </div>
                        </a>
                    </div>
                @endif
            </div>
            <div class="col-md-6">
                @if (count($invent->inventions->getMedia('other_document')) > 0)
                    <div class="card">
                        <a href="{{$invent->inventions->getMedia('other_document')->last()->getUrl()}}" target="_blank">
                            <div class="card-content">
                                @if (explode('/',$invent->inventions->getMedia('other_document')->last()->mime_type)[0] == 'image')
                                    <div class="img-res" style="background-image: url({{$invent->inventions->getMedia('other_document')->last()->getUrl()}});">

                                    </div>
                                    {{-- <img src="{{$invent->inventions->getMedia('other_document')->last()->getUrl()}}" class="card-img-top img-fluid" alt="singleminded"> --}}
                                @endif
                            </div>
                            <div class="card-footer d-flex justify-content-between">
                                <span>Other Document</span>
                                <a href="/download/other_document/{{$invent->inventions->id}}" class="btn btn-light-primary"><i class="bx bxs-download"></i></a>
                            </div>
                        </a>
                    </div>
                @endif
            </div>
        @endif
    </div>
</div>