@extends('inc.admin_asset')
@section('userActive')
    active
@endsection
@section('content')
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1 mb-0">Users</h5>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item"><a href="/admin"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active">User List
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        @include('inc.notification_display')
        <section id="basic-datatable">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body card-dashboard">
                                <div class="table-responsive">
                                    <table class="table zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>First name</th>
                                                <th>Last name</th>
                                                <th>Contact</th>
                                                <th>Email</th>
                                                <th>Active</th>
                                                <th>Delete</th>
                                            </tr>  
                                        </thead>
                                        <tbody class="table-hover">
                                            @foreach($users as $user)
                                                {{-- onclick="window.location.href='/admin/user_show/{{$user->id}}'" --}}
                                                <tr>                                
                                                    <td onclick="window.location.href='/admin/user_show/{{$user->id}}'">{{$user->firstname}}</td>
                                                    <td onclick="window.location.href='/admin/user_show/{{$user->id}}'">{{$user->lastname}}</td>
                                                    <td onclick="window.location.href='/admin/user_show/{{$user->id}}'">{{$user->phone}}</td>
                                                    <td onclick="window.location.href='/admin/user_show/{{$user->id}}'">{{$user->email}}</td>
                                                    <td onclick="window.location.href='/admin/user_show/{{$user->id}}'">
                                                        <form action="/admin/banUser" method="post">
                                                            @csrf
                                                            <input type="hidden" value="{{$user->id}}" name="user_id">
                                                            <input type="submit" class="btn {{ $user->status == 0 ? 'btn-success' : 'btn-danger'}}" value="{{ $user->status == 0 ? 'Active' : 'Banned'}}">
                                                        </form> 
                                                    </td> 
                                                    {{-- <td class="text-center"><a href="/admin/deleteUser/{{$user->id}}"><i class="bx bx-trash"></i></a></td>           --}}
                                                    <td><i class="bx bx-trash" data-toggle="modal" data-target="#delete{{$user->id}}"></i></td>
                                                    
                                                </tr>
                                                <div class="modal fade text-left" id="delete{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="rate" aria-hidden="true">
                                                    <div class="modal-dialog modal-sm modal-dialog-centered modal-dialog-centered modal-dialog-scrollable" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-body px-1 pb-0">
                                                                <section id="multiple-column-form">
                                                                    <div class="row match-height">
                                                                        <div class="col-12">
                                                                            <div class="card-content">
                                                                                <div class="del-icon text-center pb-0">
                                                                                    <i class="bx bx-trash text-danger"></i>
                                                                                    <h3 class="py-1">Are you sure?</h3>
                                                                                </div>
                                                                                <div class="card-body p-0 pb-1">
                                                                                    <form class="form" method="post" action="/admin/deleteUser">
                                                                                        @csrf
                                                                                        <input type="hidden" name="user_id" value="{{$user->id}}">
                                                                                        <div class="row">
                                                                                            <div class="col-12 d-flex justify-content-center">
                                                                                                <button type="submit" class="btn btn-danger mt-0">Delete User</button>
                                                                                            </div>

                                                                                        </div>
                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>First name</th>
                                                <th>Last name</th>
                                                <th>Contact</th>
                                                <th>Email</th>
                                                <th>Active</th>
                                                <th>Delete</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection