@extends('inc.admin_asset')
@section('hoverToOpen')
	menu-collapsed
@endsection
@section('content')
    <div class="content-header row">
    </div>
    <div class="content-body">
        <section class="page-user-profile">
            <div class="row">
                <div class="col-12">
                    <div class="">
                        <div class="card-content">
                            <div class="card-body px-0">
                                <ul class="nav user-profile-nav justify-content-center justify-content-md-start nav-tabs border-bottom-0 mb-0" role="tablist">
                                    <li class="nav-item pb-0">
                                        <a class=" nav-link d-flex px-1 active" id="feed-tab" data-toggle="tab" href="#feed" aria-controls="feed" role="tab" aria-selected="true"><i class="bx bx-home"></i><span class="d-none d-md-block">User</span></a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>

                    @include('inc.notification_display')
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="tab-content">
                                <div class="tab-pane active" id="feed" aria-labelledby="feed-tab" role="tabpanel">
                                    <div class="card">
                                        <div class="card-content">
                                            <div class="card-body">
                                                <h4 class="card-title">User details</h4>
                                                <div class="row mb-2">
                                                    <div class="col-12">
                                                        <h4><small class="text-muted">Name</small></h4>
                                                        <h6 class="font-weight-light">{{$user->firstname}} {{$user->lastname}}</h6>
                                                    </div>
                                                    <div class="col-12">
                                                        <h4><small class="text-muted">Phone</small></h4>
                                                        <h6 class="font-weight-light">{{$user->phone}}</h6>
                                                    </div>
                                                    <div class="col-12">
                                                        <h4><small class="text-muted">Email</small></h4>
                                                        <h6 class="font-weight-light">{{$user->email}}</h6>
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-between">
                                                    <form action="/admin/banUser" method="Post">
                                                        @csrf
                                                        <input type="hidden" value="{{$user->id}}" name="user_id">
                                                        <input type="submit" class="btn {{ $user->status == 0 ? 'btn-success' : 'btn-danger'}}" value="{{ $user->status == 0 ? 'active' : 'banned'}}" >
                                                        
                                                        <a href="/admin/deleteUser/{{$user->id}}" class="btn btn-danger">Delete</a>
                                                    </form>
                                                    <a class="btn btn-primary" href="{{$user->userType == 0?'/admin/inventor_show/'.$user->id: '/admin/investor/'.$user->id}}">{{$user->userType == 0?'Go to inventor page': 'Go to investor page'}}</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection