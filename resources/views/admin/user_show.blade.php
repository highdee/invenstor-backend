@extends('inc.admin_asset')
@section('hoverToOpen')
	menu-collapsed
@endsection
@section('content')
    <div class="content-header row">
    </div>
    <div class="content-body">
        <!-- page user profile start -->
        <section class="page-user-profile">
            <div class="row">
                <div class="col-12">
                    <!-- user profile heading section start -->
                    <div class="">
                        <div class="card-content">
                            <!-- user profile nav tabs start -->
                            <div class="card-body px-0">
                                <ul class="nav user-profile-nav justify-content-center justify-content-md-start nav-tabs border-bottom-0 mb-0" role="tablist">
                                    <li class="nav-item pb-0">
                                        <a class=" nav-link d-flex px-1 active" id="feed-tab" data-toggle="tab" href="#feed" aria-controls="feed" role="tab" aria-selected="true"><i class="bx bx-home"></i><span class="d-none d-md-block">User</span></a>
                                    </li>
                                    
                                </ul>
                            </div>
                            <!-- user profile nav tabs ends -->
                        </div>
                    </div>
                    <!-- user profile heading section ends -->

                    <!-- user profile content section start -->
                    @include('inc.notification_display')
                    <div class="row">
                        <!-- user profile nav tabs content start -->
                        <div class="col-lg-12">
                            <div class="tab-content">
                                <div class="tab-pane active" id="feed" aria-labelledby="feed-tab" role="tabpanel">
                                    
                                    <div class="card">
                                        <div class="card-content">
                                            <div class="card-body">
                                                <h5 class="card-title">Basic details</h5>
                                                <ul class="list-unstyled">
                                                    <h4><li><i class="bx bx-user mr-50 mb-1"></i>{{$user->fullname}}</li></h4>
                                                    {{-- <h4><li><i class="cursor-pointer bx bx-map mb-1 mr-50"></i>{{$salon->address}}</li></h4>
                                                    <h4><li><i class="cursor-pointer bx bx-map mb-1 mr-50"></i>{{$salon->city}}</li></h4> --}}
                                                    <h4><li><i class="cursor-pointer bx bx-phone-call mb-1 mr-50"></i>{{$user->phone}} </li></h4>
                                                    <h4><li><i class="cursor-pointer bx bx-envelope mb-1 mr-50"></i>{{$user->email}}</li></h4>
                                                </ul>
                                                {{-- <div class="row">
                                                    <div class="col-12">
                                                        <h6><small class="text-muted">Salon Type</small></h6>
                                                        <p> {{$salon->salon_type}}</p>
                                                    </div>
                                                    <div class="col-12">
                                                        <h6><small class="text-muted">Website</small></h6>
                                                        <p>{{$salon->websites}}</p>
                                                    </div>
                                                    <div class="col-12">
                                                        <h6><small class="text-muted">Description</small></h6>
                                                    <p>{{$salon->description}}</p>
                                                    </div>
                                                </div> --}}
                                                <div>
                                                    <form action="/admin/banUser" method="Post">
                                                        @csrf
                                                        <input type="hidden" value="{{$user->id}}" name="user_id">
                                                        <input type="submit" class="btn {{ $user->status == 0 ? 'btn-success' : 'btn-danger'}}" value="{{ $user->status == 0 ? 'active' : 'banned'}}" >
                                                        
                                                        <a href="/admin/deleteUser/{{$user->id}}" class="btn btn-danger">Delete</a>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane active" id="feed" aria-labelledby="feed-tab" role="tabpanel">
                                    
                                    <div class="card">
                                        <div class="card-content">
                                            <div class="card-body">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection