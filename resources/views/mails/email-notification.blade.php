<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <style>
        .content{
            border: 1px solid lightyellow;
            padding: 10px;
            border-radius: 3px;
        }
    </style>
</head>
<body>
    <h4>Hi {{$name}}</h4>
    <p>
        {!! $content !!}
    </p>

    @if(isset($link))
        <a href="{{$link}}">{{$link_text}}</a>
    @endif
</body>
</html>
