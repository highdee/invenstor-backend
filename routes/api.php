<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('/v1/')->group(function(){

//    PUBLIC ROUTES
    Route::post('create-account', 'loginController@createAccount');
    Route::post('login', 'loginController@login');
    Route::get('verify-account', 'loginController@verify_account');
    Route::get('get-dashboard',"userController@get_dashboard");
    Route::get('resend-verification', "userController@resend_verification");
    Route::post('forget-password',"loginController@forget_password");
    Route::post('reset-password',"loginController@reset_password");
    Route::get('verify-reset-token',"loginController@verify_password");

    Route::get('get-invention/{reference}', 'userController@getInvention');
    Route::get('get-reviews/{reference}', 'userController@getReviews');
    Route::post('give-review/{reference}', 'userController@sendReview');


    Route::post('save-progress','InventionController@saveProgress');
    Route::get('getSavedData', 'InventionController@getSavedData');
    Route::post('submitForReview', 'InventionController@submitForReview');
    Route::get('getUserInventions', 'InventionController@getUserInventions');
    Route::get('getInvention/{id}', 'InventionController@getInvention');

    Route::get('fetch-inventions', 'InvestorController@fetchInventions');
    Route::get('attach-category/{id}', 'InvestorController@attachCategory');
    Route::get('detach-category/{id}', 'InvestorController@detachCategory');


    Route::apiResources([
        'category'=>'CategoryController',
    ]);
});
