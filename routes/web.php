<?php


Route::get('/clear', function(){
    \Artisan::call('cache:clear');
    \Artisan::call('config:cache');
    \Artisan::call('config:clear');
});


Auth::routes();

Route::get('adminLogin', 'adminLoginController@show');
Route::post('adminLogin', 'adminLoginController@login');

Route::prefix('/admin')->group(function(){
    Route::get('/', 'adminController@index');
    Route::get('/inventor', 'adminController@inventor');
    Route::get('/inventor_show/{user_id}','adminController@inventor_show');
    Route::get('/inventions', 'adminController@inventions');
    Route::get('/invention/{id}', 'adminController@invention');
    Route::get('/investors', 'adminController@investors');
    Route::get('/investor/{user_id}', 'adminController@investor');
    Route::get('/userList', 'adminController@userList')->name('admin.user');
    Route::get('/user_show/{id}', 'adminController@user_show');
    Route::post('/deleteUser', 'adminController@deleteUser');
    Route::post('/banUser', 'adminController@banUser');
    Route::get('/adminLogout', 'adminController@logout');
    Route::post('/rejectInvention','adminController@rejectInvention');
    Route::post('/cancelInvention','adminController@cancelInvention');
    Route::post('/acceptInvention','adminController@acceptInvention');
});

Route::get('/download/{type}/{id}', 'adminController@download');
// Route::view('test','mails.acceptInvention');