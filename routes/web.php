<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('welcome');
//    dd(env('MAIL_USERNAME'));
dd(env('APP_URL_FRONTEND'));
});

Route::get('/clear', function(){
    \Artisan::call('cache:clear');
    \Artisan::call('config:cache');
    \Artisan::call('config:clear');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/me', function(){
    // $note = App\Note::where('invention_id',1)
    //                 ->where('status',1)
    //                 ->first();
    
    
    // dd('kekekkk');
});

Auth::routes();

Route::get('adminLogin', 'adminLoginController@show');
Route::post('adminLogin', 'adminLoginController@login');

Route::prefix('/admin')->group(function(){
    Route::get('/', 'adminController@index');
    Route::get('/inventor', 'adminController@inventor');
    Route::get('/inventor_show/{id}','adminController@inventor_show');
    Route::get('/invention', 'adminController@invention');
    Route::get('/investor', 'adminController@investor');
    Route::get('/userList', 'adminController@userList')->name('admin.user');
    Route::get('/user_show/{id}', 'adminController@user_show');
    Route::get('/deleteUser/{id}', 'adminController@deleteUser');
    Route::post('/banUser', 'adminController@banUser');
    Route::get('/adminLogout', 'adminController@logout');
    Route::post('/rejectInvention','adminController@rejectInvention');
    Route::post('/cancelInvention','adminController@cancelInvention');
    Route::post('/acceptInvention','adminController@acceptInvention');
});

Route::get('/download/{type}/{id}', 'adminController@download');
Route::view('test','mails.acceptInvention');